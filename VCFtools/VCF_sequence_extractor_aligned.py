# -*- coding: utf-8 -*-
"""
Created on Fri Nov 21 15:27:32 2014

@author: Billie
"""
## this script should take a list of gene regions and write out all the consensus sequences for each indv in a vcf.
#chromosome specific gene list: genename chrom genestart geneend
#imput VCF does NOT need to contain every base on the chromosome, be compressed using bgzip, and indexed with tabix
#useage: script inputVCF.gz genelist DP(indiv)cutoff
# script will NOT insert ?s for missing bases but does insert Ns for missing genotype calls.  use with snps only vcf or vcf with all bases in genome for insertions and deletions, script inserts gaps "-" equivalent to the allele with the longest length. output sequences are 'aligned' to the reference and ready for distance calculations/tree building.

import sys
#sys.path.append("/Users/Billie/Desktop/GENETICS_SOFTWARE/PyVCF-master")
sys.path.append("/data/billie.gould/SCRIPTS/PyVCF-master/")
import vcf
import pysam

iupac_dict={'A/A':'A','T/T':'T','C/C':'C','G/G':'G', "./.":'N', 'A/T':'W', 'A/C':'M' ,'A/G':'R' ,'T/C':'Y' ,'T/G':'K' ,'C/G':'S', 'T/A':'W', 'C/A':'M' ,'G/A':'R' ,'C/T':'Y' ,'G/T':'K' ,'G/C':'S'}

#for testing: reader = vcf.Reader(open("test_individ_vcf.txt", "r"))

def construct(section, genestart, geneend, samples):
    prev_pos=int(genestart)-1
    dup_pos=0
    lines_processed=0
    for line in section:
        lines_processed=lines_processed+1
        #if int(line.POS)%100 == 0:
	    #print str(line)
        if genestart <= line.POS <= geneend:  # if the line position is within the gene , 
            if line.POS == prev_pos: #if a duplicate position/variant exists use the last one
                #dup_pos=dup_pos +1
                prev_pos=line.POS
                continue #read in next item/line in the chrom list
            elif line.POS > prev_pos:
		print "SNP: " + str(line.REF) + "\t" + str(line.ALT[0])
	    	if str(line.ALT[0]) == 'None':
                    for s in samples:
                        sequence=seq_construc_dict[s]
			#print(line.genotype(s).data)
                        if (line.genotype(s).gt_bases) == None or line.genotype(s)['DP'] < dp_cutoff : #if there is no base call or low seq depth insert Ns
                            sequence.append("N")
                        else:
                            base = iupac_dict.get(line.genotype(s).gt_bases)
                            if base == None: print str(base)
			    #print "call:" + str(line.genotype(s).gt_bases) + "  snp base: " + str(base) + "type: " + str(line.genotype(s).gt_type)
                            sequence.append(base)
			    #print "appending iupac base: " + str(base)
                elif len(line.ALT[0]) > len(line.REF):		#for insertion 
		    print "insert detected"
		    newRefbase = line.REF + ("-" * (len(line.ALT[0]) - len(line.REF)))
		    print "ref: " + str(newRefbase) + "  alt: " + str(line.ALT[0])
		    position_length = len(line.ALT[0])
		    for s in samples:               #add the genotype info for that line to the sample sequences for the gene
                        sequence=seq_construc_dict[s]
                        if (line.genotype(s).gt_bases) == None or line.genotype(s)['DP'] < dp_cutoff : #if there is no base call or low seq depth insert Ns
                            sequence.append("N" * (position_length))
                        elif line.genotype(s).gt_type == 0: #homozy ref 
                            sequence.append(str(newRefbase))
			elif line.genotype(s).gt_type > 0: #het or homzy alt, append ALT 
                            sequence.append(str(line.ALT[0]))

		elif len(line.ALT[0]) < len(line.REF):		#for deletions
		    print "deletion detected"
		    newAltbase = str(line.ALT[0]) + ("-" * (len(line.REF) - len(line.ALT[0])))
		    print "ref: " + str(line.REF) + "  alt: " + str(newAltbase) 
		    position_length = len(line.REF)
                    for s in samples:               #add the genotype info for that line to the sample sequences for the gene
                        sequence=seq_construc_dict[s]
                        if (line.genotype(s).gt_bases) == None or line.genotype(s)['DP'] < dp_cutoff : #if there is no base call or low seq depth insert Ns
                            sequence.append("N" * (position_length))
                        elif line.genotype(s).gt_type == 0: #homozy ref 
                            sequence.append(str(line.REF))  
                        elif line.genotype(s).gt_type > 0: #het or homzy alt
                            sequence.append(str(newAltbase))
                
		else:                                  # for SNPs
		    print "SNP: " + str(line.REF) + "\t" + str(line.ALT)
		    for s in samples:
			sequence=seq_construc_dict[s] 
			if (line.genotype(s).gt_bases) == None or line.genotype(s).data[2] == None : #if there is no base call or low seq depth insert Ns
                            sequence.append("N")
		        else:
		            base = iupac_dict.get(line.genotype(s).gt_bases)
		            if base == None: sequence.append(line.genotype(s).gt_bases.split("/")[1]) #print "snp base: " + str(base) + "type: " + str(line.genotype(s).gt_type)
			    else: sequence.append(base)
                prev_pos=line.POS
                #print "duplicate poitions: " +str(dup_pos)
    print "lines processed: " +str(lines_processed)
#######################################
prev_pos=0

reader = vcf.Reader(open(sys.argv[1], "r"))
samples=reader.samples

genelist=open(sys.argv[2], "r")

dp_cutoff=int(sys.argv[3])

for gene in genelist:           #chromosome specific gene list: genename chrom genestart geneend
    print str(gene)
    genename = gene.split()[0]      #create a dictiosamples=reader.samplesnary of empty sequence lists for each sample for the gene
    print "Processing: " + str(genename)
    chrom=gene.split()[1]
    genestart=int(gene.split()[2])
    geneend=int(gene.split()[3])
    seq_construc_dict={}
    for s in samples:
        seq_construc_dict[s]=([])
    
    section = reader.fetch(chrom, (genestart-1), geneend)	#retrieve vcflines within the gene
    construct(section, genestart,geneend,seq_construc_dict)  #use this function to construct all the consensus sequences by adding to seq_construc_dict
    
    outfile= (str(genename) + "_snps_only.fa")
    output=open(outfile, "w")
    for seq in seq_construc_dict.items():
        sample=seq[0]        
        print "sample: " + str(seq[0]) + "  seq: " + str(seq[1])
	polymer=''.join(seq[1])  
        output.write(">" + str(sample) + "_" + str(genename) + "\n" + str(polymer) + "\n")
    output.close()
    print "Gene extracted: " + str(genename)
    
genelist.close()

