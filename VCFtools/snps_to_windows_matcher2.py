# -*- coding: utf-8 -*-
"""
Created on Wed Jul  2 11:25:39 2014

@author: Billie
"""

# This script will match variants in a VCF to regions listed in a GFF file. GFF and VCF must be sorted by coordinate.

# Prints to STD OUT

# useage: script VCF.vcf regions.gff


import sys

#create a dictionary with a list of regions per chromosome

GFF = open(sys.argv[2], "r")
windict = {}

for line in GFF:
    chrm = int(line.split()[0].strip())
    start = int(line.split()[3])
    end = int(line.split()[4])
    name = line.split()[8][3:15]

    if chrm not in windict:
        windict[chrm] = [(name, start, end)]
    elif chrm in windict:
        windict[chrm].append((name, start, end))

#print("KEYS: " + repr(min(windict.keys())))

GFF.close()

sys.stdout.write("CHR" + "\t" + "SNP.POS" + "\t" + "REG.NAME" + "\t"+ "REG.START" + "REG.END" + "\n")
#for snp in VCF, retieve correct chr RegRegion list, see if it falls in any of them 

counter = 0
VCF=open(sys.argv[1], "r")
for line in VCF:
    counter += 1
    snp=line.split()
    if counter == 10000:
        sys.stderr.write("Processing variant: " + str(snp[0:2]))
        counter = 0
    if "#" in snp[0]:
        continue
    else:
        snpchr=int(snp[0].strip())
        snppos=int(snp[1].strip())
        #print "SNP: " + str(snpchr) + "\t" + str(snppos)
    try:
        windowlist=windict[snpchr]
    except KeyError:
        sys.stderr.write("No genes on Chr: " + str(snpchr)+"\n")
    else:
#COMPARE EACH SNP TO ALL WINDOWS IN CHR
        for window in windowlist:

            windowname = window[0]
            #print "gene: " + str(windowname)
            windowstart=window[1]
            windowend=window[2]

            if snppos < windowstart:
                break                                   # only stop comparing when the next window starts downstream of the snp
                #print "snp " + str(snppos) + " non-overlapping with window " + str(windowstart)            
            elif snppos >= windowstart and snppos <= windowend:
                #print "MATCH"
                sys.stdout.write(str(snpchr) + "\t" + str(snppos) + "\t" + str(windowname) + "\t"+ str(windowstart) + "\t" + str(windowend) + "\n")
                # continue with the next window, which may overlap the current window
            elif snppos > windowend:
                continue

sys.stderr.write("Complete. \n")
VCF.close()
