# This script will read in a VCF file and output only records that meet the cutoff value for percent of calls that are heterozygous.

import sys


sys.path.append("/Users/Billie/Desktop/GENETICS_SOFTWARE/PyVCF-master")
import vcf


P = float(sys.argv[2])  # the percent heterozygosity cutoff

VCF = vcf.Reader(open(sys.argv[1], 'r'))

out_vcf = vcf.Writer(open((str(P) + "_hetfilt.vcf"), "w"), VCF)

removed = 0
passed = 0

for record in VCF:
    site_perc_het = record.num_het/float(record.num_called)
    if  site_perc_het > P:
        removed += 1
        continue
    elif site_perc_het <= P:
        passed += 1
        out_vcf.write_record(record)

tot_sites = removed + passed

print "Total sites: " + str(tot_sites)
print "removed: " + str(removed)
print "kept: "  + str(passed)

