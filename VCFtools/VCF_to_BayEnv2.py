# -*- coding: utf-8 -*-
"""
Created on Tue May 27 10:59:51 2014

@author: Billie
"""

import sys
import re

print "this script will tabluste the ref and alt allele frequencies for each line in the input vcf. output includes chr pos and snp number which must be removed before inpput to BayEnv. generate one vcf per population group and run script for each one. paste outputs"
"useage: python VCF_to_BayEnv2.py input.vcf outfilename"

output=open(sys.argv[2], "w")
output.write("SNP_NUMBER" + "\t" + "CHR" + "\t" + "POS" + "\t" + "ALLELE_COUNT\n")
vcf=open(sys.argv[1], "r")

snpnumber=0
for line in vcf:
    if "#" in line:
        continue
    else:
        snpnumber=snpnumber+1
        data=re.split('\t|:', line)
        A=data.count("0/0")
        B=data.count("0/1")
        C=data.count("1/1")
        ref= (2*A)+B
        alt= (2*C)+B
        output.write(str(snpnumber) + "\t" + str(data[0]) + "\t" + str(data[1]) + "\t" + str(ref) + "\n")
        output.write(str(snpnumber) + "\t" + str(data[0]) + "\t" + str(data[1]) + "\t" + str(alt) + "\n")

print "Processed " + str(snpnumber) + " variants"

vcf.close()
output.close()

    
