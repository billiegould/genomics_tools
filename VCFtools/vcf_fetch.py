# this script will import a vcf file and fetch snps from the given interval into a new file. this should be much faster than the vcftools query function.
# Note: vcf must be compressed with bgzip and indexed with tabix

import sys
import vcf

vcf_reader = vcf.Reader(open(sys.argv[1], "r"))

chrom = (sys.argv[2])
start = int(sys.argv[3])
end = int(sys.argv[4])

vcf_writer = vcf.Writer(open(sys.argv[5], "w"), vcf_reader)

interval = vcf_reader.fetch(chrom, start, end)

for record in interval:
    vcf_writer.write_record(record)

print "Fetch complete"
