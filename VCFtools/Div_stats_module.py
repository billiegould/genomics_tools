import sys
import math
from statistics import variance


##### INITIALIZE MODULE #####################

pi_list_IA=[]
pi_list_CP=[]
freq_list_IA=[]
freq_list_CP=[]
dxy_list=[]
depth_list=[]
G_list=[]
Fst_list=[]
positions=[]

##############################################



def G_calculator(freqa0, freqa1, depthPop1, depthPop2, a0_pop1, a0_pop2, a1_pop1, a1_pop2):
    #expected frequency if no difference between pops
    expect_a0p1 = freqa0 * depthPop1
    expect_a1p1 = freqa1 * depthPop1
    expect_a0p2 = freqa0 * depthPop2
    expect_a1p2 = freqa1 * depthPop2

    #Calc G Statistic: Ratio of observed to expected.
    #filter based on expected frequency.
    if expect_a0p1 > 2 and expect_a1p1 > 2 and expect_a0p2 > 2 and expect_a1p2 > 2: #cutoff recommended in the Magwene 2011 paper.

        if a0_pop1 != 0:
            G_a0p1 = a0_pop1 * math.log(a0_pop1/expect_a0p1)
        else:
            G_a0p1 = 0

        if a1_pop1 != 0:
            G_a1p1 = a1_pop1 * math.log(a1_pop1/expect_a1p1)
        else:
            G_a1p1 = 0

        if a0_pop2 != 0:
            G_a0p2 = a0_pop2 * math.log(a0_pop2/expect_a0p2)
        else:
            G_a0p2 = 0

        if a1_pop2 != 0:
            G_a1p2 = a1_pop2 * math.log(a1_pop2/expect_a1p2)
        else:
            G_a1p2 = 0

        G = 2 * (G_a0p1 + G_a1p1 + G_a0p2 + G_a1p2)
    else:
        G = 0  # if expected allele freq in one pop is <2, then overall freq of one allele approaches zero and G approaches 0.
    return G


def Fst_asTheta(a0_pop1, a0_pop2, a1_pop1, a1_pop2):     #this might not be quite right . . .

    p1 = float(a1_pop1) / (a1_pop1 + a0_pop1)      # frequency of alt allele in group 1
    p2 = float(a1_pop2) / (a1_pop2 + a0_pop2)      # frequency of alt allele in group 2
    q1 = 1-float(p1)
    q2 = 1-float(p2)

    var =  variance([p1,p2,q1,q2])   # variance in allele frequencies across populations
    mean = (p1+p2+q1+q2)/4

    theta = var/(mean*(1-mean))      # a formulation of Wright's Fst, see Holsinger and Weir 2009 review.

    return theta



def Fst_ExpHet(a0_pop1, a0_pop2, a1_pop1, a1_pop2):

    p1 = float(a1_pop1) / (a1_pop1 + a0_pop1)      # frequency of alt allele in group 1
    p2 = float(a1_pop2) / (a1_pop2 + a0_pop2)      # frequency of alt allele in group 2
    pt = float(a1_pop1 + a1_pop2)/(a1_pop1 + a0_pop1 + a1_pop2 + a0_pop2)
    q1 = 1-float(p1)
    q2 = 1-float(p2)
    qt = 1 - pt

    HT=2*pt*qt
    HS_pop1 = 2*p1*q1
    HS_pop2 = 2*p2*q2
    site_Fst = (HT - ((HS_pop1+HS_pop2)/2))/HT

    return site_Fst


## this function for use specifically in calculating population genetic stats from SNAPE snp files, Mimulus pooled-seq 2015
def calc_stats(line):
    try:
        last_field = line.split()[21]
        prob_snp_IA = float(line.split()[8])
        prob_snp_CP = float(line.split()[19])
    except IndexError: #this exception occurs if there are not enough fields or there is a "*" value at this position in the snape input file.
        return

# numer of chromosomes sampled per locus for each pool (2 * n for diploids)
    n_IA = 184
    n_CP = 204

    #allele count per population
    a0_pop1 = float(line.split()[3]) #coverage_ReferanceAllele_Pop1
    #print (str(a0_pop1))
    a1_pop1 = float(line.split()[4]) #covAltPop1
    #print (str(a1_pop1))
    a0_pop2 = float(line.split()[14]) #covRefPop2
    #print (str(a0_pop2))
    a1_pop2 = float(line.split()[15]) #covAltPop2
    #print (str(a1_pop2))

    depthPop1 = a0_pop1 + a1_pop1
    #print ("Pop 1 depth: " + str(depthPop1))
    depthPop2 = a0_pop2 + a1_pop2
    #print ("Pop 2 depth: " + str(depthPop2))
    tot_depth = depthPop1 + depthPop2

    if depthPop1<=50 or depthPop2<=50:  #filter out low coverage snps
        return

    if 0.05 < prob_snp_IA < 0.95 and 0.05 < prob_snp_CP < 0.95: #consider these positions as missing data #for pooled seq there is NO info on heterozygosity
        return
    else:                                   #calculate statistics for non missing bases
        #global scaff
        #scaff=line.split()[0]
        pos=int(line.split()[1])

        #expected Alt allele frequencies for each pop from SNAPE, will be 0 for invariant reference sites
        freq_exp_IA = float(line.split()[10])
        freq_exp_CP = float(line.split()[21])

        #calc frequency of each allele across both pops
        freqa0 = ( a0_pop1 + a0_pop2 ) / (depthPop1 + depthPop2)  #frequency of REF read counts in combined pop
        freqa1 = ( a1_pop1 + a1_pop2 ) / (depthPop1 + depthPop2) #frequency of ALT read counts in combined pop

        # Calculate divergence statistics
        site_pi_IA = 2 * freq_exp_IA * (1-freq_exp_IA) * (n_IA/(n_IA - 1))  #Tajima 1989
        site_pi_CP = 2 * freq_exp_CP * (1-freq_exp_CP) * (n_CP/(n_CP - 1))

        site_dxy = (freq_exp_CP * (1-freq_exp_CP)) + (freq_exp_IA * (1-freq_exp_CP)) #Nei 1987, eq 10.20

        site_depth = int(line.split()[3]) + int(line.split()[4]) + int(line.split()[14]) + int(line.split()[15])  #depth reference plus depth alt in IA and CP respectively.

        site_G = G_calculator(freqa0, freqa1, depthPop1, depthPop2, a0_pop1, a0_pop2, a1_pop1, a1_pop2)

        if freqa1 == 0 or freqa0 == 0:
            site_Fst = 0
        else:                                                   # this is Fst based on expected heterozygosity
            obs_freqa1_CP = a1_pop2/(a0_pop2 + a1_pop2)
            obs_freqa1_IA = a1_pop1/(a0_pop1 + a1_pop1)
            HT=2*freqa1*freqa0
            HS_CP = 2*obs_freqa1_CP*(1-obs_freqa1_CP)
            HS_IA = 2*obs_freqa1_IA*(1-obs_freqa1_IA)
            site_Fst = (HT - ((HS_IA+HS_CP)/2))/HT

            results = [pos, freq_exp_IA, freq_exp_CP, site_pi_IA, site_pi_CP, site_depth, site_Fst, site_G, site_dxy]

            if any([math.isnan(i) for i in results]):
                return
            else:
                #print "OK"
                return (pos, freq_exp_IA, freq_exp_CP, site_pi_IA, site_pi_CP, site_depth, site_Fst, site_G, site_dxy)