## This script will take a varscan file of format:
#chr	pos	REF	ALT	POOLCALL(Cons:Cov:Reads1:Reads2:Freq:P-value)	STRANDBIAS(R1+:R1-:R2+:R2-:pval)	samplesRefHom	samplesHET	samples AltHom	samplesNC	cons:cov:readsR:readsV:Freq:pval(prob of snp)

## and convert it to VCF format:   Note: sites not passing the strand bias at 0.95 and non biallelic sites are excluded from the vcf. Assumes all indel calls are homozygous.

#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT  GT1 GT2 . . .GTn
#scaffold_1	272	.	T	C	437.94	PASS	.	GT:DP	0/0:33  . . .

#Useage: script input.snp output.vcf   #adjust script for number of samples in snp file.

import sys
import math


def parse_GT_info(varscan_gt_data, vsREF, vsALT):
    print varscan_gt_data[0][0]

    if varscan_gt_data[0][0] == "*":       #genotype calls that are heterozygous indels
        GT = "0/1"
    if varscan_gt_data[0][0] == "-" or varscan_gt_data[0][0] == "+":        #genotype calls that are homozyg indels
        GT = "1/1"
    if varscan_gt_data[0] == vsREF:     #genotype calls that are homozyg reference
        GT = "0/0"
    elif varscan_gt_data[0] == vsALT[0]:   #genotype calls that are homozyg alt snp
        GT = "1/1"
    elif varscan_gt_data[0] == "N":    #genotype calls that are missing or ambiguous.
        GT = "./."
    elif varscan_gt_data[0] in ["R","Y","S","W","K","M"]:        #genotype calls that are heterozygous snps
        GT = "0/1"

    DP = varscan_gt_data[1]

    vcf_gt_data = '%s:%s' % (GT,DP)
    return vcf_gt_data

######################################################

header = ["#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "FORMAT", "AOZXB",	"NNUO",	"AOZXA",	"NNUN",	"APBGU",	"NNUP",	"NNUS",	"NNUT",	"NNUU",	"NNUW",	"NUOY",	"NZWC",	"NZWG",	"NZWN",	"NZWO",	"NZWP",	"NZWS",	"NZWT",	"NZWU",	"NZWW",	"NZXS",	"NZWX",	"NZWY",	"NZXT",	"NZXU",	"NZXW",	"NZXX",	"NZXY",	"PAGX",	"PAGY",	"UAGG",	"UAGH",	"UAGN",	"UAGO",	"UAGP",	"UAGS",	"UAGT",	"UAGU",	"UAGW",	"UAGX",	"UAGY",	"UAGZ",	"UAHA",	"UAHB",	"UAHC",	"UAHG",	"UAHH",	"UAHN",	"WZGS",	"WZGT",	"WZGU",	"WZGW",	"WZGX",	"WZGY",	"WZGZ",	"WZHA",	"WZHB",	"PIN1",	"CNF2",	"LAR1",	"POI3",	"IAUP",	"IAUS",	"IAUX",	"IAUY",	"IAUZ",	"IAWN",	"IAWO",	"IAWP",	"IAWS",	"IAWU",	"IAWX",	"YAZH",	"AOZWN",	"AOZWO",	"AOZWP",	"AOZWS",	"AOZWT",	"AOZWU",	"AOZWW",	"AOZWX",	"AOZWY",	"AOZWZ",	"IFZO",	"EPS1",	"AOZXC",	"AOZXG",	"ICII",	"COR18"]

varscan_file=open(sys.argv[1], "r")

vcf = open(sys.argv[2], "w")
vcf.write("\t".join(header) + "\n")

for line in varscan_file:

    #print(line.split())

    sbias_pval = float(line.split()[5].split(":")[-1])       # filter out sites with strand bias
    if sbias_pval <= 0.95:
        continue


    CHR = line.split()[0]
    POS = line.split()[1]
    ID = "."
    vsREF = line.split()[2]
    vsALT = line.split()[3].split(",")

    if len(vsALT) > 1:                              #skip non-biallelic sites
        continue
    elif vsALT[0][0] == "+":                        #for insertions, adjust ALT to include the reference base, see VarScan documentation
        vcfREF = vsREF
        vcfALT = vsREF + vsALT[0][1:]
    elif vsALT[0][0] == "-":                        #for deletions, adjust REF to include alt bases
        vcfALT = vsREF
        vcfREF = vsREF + vsALT[0][1:]
    else:
        vcfREF = vsREF
        vcfALT = vsALT[0]

    pval = float(line.split()[4].split(":")[5])
    if pval == 0:
        QUAL = "255"
    else:
        QUAL = round((-10 * math.log10(pval)), 1)
    if QUAL > 255:
        QUAL = "255"

    FILTER = "PASS"
    INFO = "."
    FORMAT = "GT:DP"

    vcf_line=[CHR,POS,ID,vcfREF,vcfALT,str(QUAL),FILTER,INFO,FORMAT]
    #print [vsREF,vsALT,vcfREF,vcfALT]
    #for each genotype (samples(+1)) on the vcf line, interpret the genotype information and append to vcf data list
    for i in range(10,14,1):
        varscan_gt_data = line.split("\t")[i].split(":")
        vcf_gt_data = parse_GT_info(varscan_gt_data, vsREF, vsALT)
        vcf_line.append(vcf_gt_data)

    vcf.write("\t".join(vcf_line) + "\n")

vcf.close()
varscan_file.close()