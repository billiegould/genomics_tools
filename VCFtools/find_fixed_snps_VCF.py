# This script will read in a VCF file and output only records that are fixed variants between the two input groups to new VCF.

# python find_fixed_snps_VCF.py pop1.txt pop2.txt infile.vcf outfile_name

import sys

#sys.path.append("/Users/Billie/Desktop/GENETICS_SOFTWARE/PyVCF-master")
import vcf

group1_file = open(sys.argv[1], "r")
group1 = []
for pop1 in group1_file:
    group1.append(pop1.rstrip())
sys.stderr.write(str(len(group1)) + " populations in group 1 \n")
group1_file.close()

group2_file = open(sys.argv[2], "r")
group2 = []
for pop2 in group2_file:
    group2.append(pop2.rstrip())
sys.stderr.write(str(len(group2)) + " populations in group 2 \n")
group2_file.close()

VCF = vcf.Reader(open(sys.argv[3], 'r'))

out_vcf = vcf.Writer(open(sys.argv[4], "w"), VCF)
sites_processed = 0
fixed_sites = 0

for record in VCF:

    g1_aac = 0
    g1_rac = 0
    g2_aac = 0
    g2_rac = 0

    for indv1 in group1:
        if record.genotype(indv1)['GT'] == "0/0":
            g1_rac += 2
        if record.genotype (indv1)['GT'] == "0/1":
            g1_rac += 1
            g1_aac += 1
        if record.genotype(indv1)['GT'] == "1/1":
            g1_aac += 2
        if record.genotype(indv1)['GT'] == "./.":
            continue

    for indv2 in group2:
        if record.genotype(indv2)['GT'] == "0/0":
            g2_rac += 2
        if record.genotype (indv2)['GT'] == "0/1":
            g2_rac += 1
            g2_aac += 1
        if record.genotype(indv2)['GT'] == "1/1":
            g2_aac += 2
        if record.genotype(indv2)['GT'] == "./.":
            continue

    g1_aaf = float(g1_aac)/(g1_aac + g1_rac)
    g1_raf = 1 - float(g1_aaf)

    g2_aaf = float(g2_aac)/(g2_aac + g2_rac)
    g2_raf = 1 - float(g2_aaf)

    if g1_aaf == 1 and g2_aaf == 0:
        out_vcf.write_record(record)
        fixed_sites += 1
    if g1_raf == 1 and g2_raf == 0:
        out_vcf.write_record(record)
        fixed_sites += 1

    sites_processed += 1

    if sites_processed % 100000 == 0:
        sys.stderr.write(str(sites_processed) + " sites processed . . . \n")

sys.stderr.write("Complete. There are " + str(fixed_sites) + " fixed sites.")

