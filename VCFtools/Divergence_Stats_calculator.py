import sys
import math

#print "NOTE: Run this script on one snape file per chromosome (snps only). Useage: script.py infile. Writes to StdOut. "

# numer of chromosomes sampled per locus for each pool (2 * n for diploids)
n_IA = 184
n_CP = 204


################################################################

def G_calculator(freqa0, freqa1, depthPop1, depthPop2):
    #expected frequency if no difference between pops
    expect_a0p1 = freqa0 * depthPop1
    expect_a1p1 = freqa1 * depthPop1
    expect_a0p2 = freqa0 * depthPop2
    expect_a1p2 = freqa1 * depthPop2

    #Calc G Statistic: Ratio of observed to expected.
    #filter based on expected frequency.
    if expect_a0p1 > 2 and expect_a1p1 > 2 and expect_a0p2 > 2 and expect_a1p2 > 2: #cutoff recommended in the Magwene 2011 paper.

        if a0_pop1 != 0:
            G_a0p1 = a0_pop1 * math.log(a0_pop1/expect_a0p1)
        else:
            G_a0p1 = 0

        if a1_pop1 != 0:
            G_a1p1 = a1_pop1 * math.log(a1_pop1/expect_a1p1)
        else:
            G_a1p1 = 0

        if a0_pop2 != 0:
            G_a0p2 = a0_pop2 * math.log(a0_pop2/expect_a0p2)
        else:
            G_a0p2 = 0

        if a1_pop2 != 0:
            G_a1p2 = a1_pop2 * math.log(a1_pop2/expect_a1p2)
        else:
            G_a1p2 = 0

        G = 2 * (G_a0p1 + G_a1p1 + G_a0p2 + G_a1p2)
    else:
        G = 0  # if expected allele freq in one pop is <2, then overall freq of one allele approaches zero and G approaches 0.
    return G

#####################################



snape_file = open(sys.argv[1], "r") #this file is the merged snape file:
#Input format: IA stats are listed in each row followed by CP stats
#CHR	POS	REF.ALLELE	 #REF.NUCL	  #ALT.NUCL	QUAL.REF	QUAL.ALT	MOST.FREQUENT.NUCL	1-PROB.0(prob of snp)	PROB.1	EXPECT.FREQ
#scaffold_1	1	T	35	0	72	72	T	0.0451	1.117e-138	0.0009034 scaffold_1	1	T	26	0	71	71	T	0.05264	5.553e-101	0.001206
#scaffold_1	2	A	35	0	72	72	A	0.0451	1.117e-138	0.0009034 scaffold_1	2	A	32	0	70	70	A	0.04716	1.406e-120	0.0009795
#scaffold_1	3	G	35	0	71	71	G	0.0451	3.537e-135	0.0009034 scaffold_1	3	G	34	0	70	70	G	0.04559	5.609e-128	0.0009212

sys.stdout.write("scaff" + "\t" + "win.start" + "\t" + "win.end" + "\t" + "freqCP" + "\t"+ "freqIA" + "\t" + "num_snps" + "\t" + "piCP"+ "\t" + "piIA" + "\t" + "Dxy_win" +"\t"+ "win_G" +"\t"+ "win_Fst" +"\t"+ "missing_data" + "\t" + "win_depth" +"\n" )
#set window lists to zero
pi_list_IA=[]
pi_list_CP=[]
freq_list_IA=[]
freq_list_CP=[]
dxy_list=[]
depth_list=[]
positions=[]
G_list=[]
Fst_list=[]
called_bases = 0
num_snps = 0

for line in snape_file:
    print line
    try:
        last_field = line.split()[21]
        prob_snp_IA = float(line.split()[8])
        prob_snp_CP = float(line.split()[19])
    except IndexError: #this exception occurs if there are not enough fields or there is a "*" value at this position in the snape input file.
        continue

    #allele count per population
    a0_pop1 = float(line.split()[3]) #coverage_ReferanceAllele_Pop1
    #print (str(a0_pop1))
    a1_pop1 = float(line.split()[4]) #covAltPop1
    #print (str(a1_pop1))
    a0_pop2 = float(line.split()[14]) #covRefPop2
    #print (str(a0_pop2))
    a1_pop2 = float(line.split()[15]) #covAltPop2
    #print (str(a1_pop2))

    depthPop1 = a0_pop1 + a1_pop1
    #print ("Pop 1 depth: " + str(depthPop1))
    depthPop2 = a0_pop2 + a1_pop2
    #print ("Pop 2 depth: " + str(depthPop2))
    tot_depth = depthPop1 + depthPop2

    if depthPop1 <= 50 or depthPop2<=50:  #filter out low coverage snps
        print "Low depth"
        continue

    if 0.05 < prob_snp_IA < 0.95 and 0.05 < prob_snp_CP < 0.95: #consider these positions as missing data
        print "Missing data"
        continue

    else:
        global scaff
        scaff=line.split()[0]
        pos=int(line.split()[1])

        if prob_snp_IA > 0.95 or prob_snp_CP > 0.95:    # record position and count called bases and snps
            called_bases += 1
            num_snps += 1
        elif prob_snp_IA < 0.05 or prob_snp_CP < 0.05:
            called_bases += 1

        print "Called bases: " + str(called_bases)
        print "SNPs in window: " + str(num_snps)

        #expected Alt allele frequencies for each pop from SNAPE, will be 0 for invariant reference sites
        freq_exp_IA = float(line.split()[10])
        freq_exp_CP = float(line.split()[21])

        #calc frequency of each allele across both pops
        freqa0 = ( a0_pop1 + a0_pop2 ) / (depthPop1 + depthPop2)  #frequency of REF read counts in combined pop
        freqa1 = ( a1_pop1 + a1_pop2 ) / (depthPop1 + depthPop2) #frequency of ALT read counts in combined pop


        # Calculate divergence statistics
        site_pi_IA = 2 * freq_exp_IA * (1-freq_exp_IA) * (n_IA/(n_IA - 1))  #Tajima 1989
        site_pi_CP = 2 * freq_exp_CP * (1-freq_exp_CP) * (n_CP/(n_CP - 1))

        site_dxy = (freq_exp_CP * (1-freq_exp_CP)) + (freq_exp_IA * (1-freq_exp_CP)) #Nei 1987, eq 10.20

        site_depth = int(line.split()[3]) + int(line.split()[4]) + int(line.split()[14]) + int(line.split()[15])  #depth reference plus depth alt in IA and CP respectively.

        site_G = G_calculator(freqa0, freqa1, depthPop1, depthPop2)


        # Calculate Fst
        if freqa1 == 0 or freqa0 == 0:
            site_Fst = 0
        else:
            obs_freqa1_CP = a1_pop2/(a0_pop2 + a1_pop2)
            obs_freqa1_IA = a1_pop1/(a0_pop1 + a1_pop1) 
            HT=2*freqa1*freqa0
            HS_CP = 2*obs_freqa1_CP*(1-obs_freqa1_CP)
            HS_IA = 2*obs_freqa1_IA*(1-obs_freqa1_IA)
            site_Fst = (HT - ((HS_IA+HS_CP)/2))/HT 
 
        #write statistics to window lists
        pi_list_IA.append(site_pi_IA)
        pi_list_CP.append(site_pi_CP)

        freq_list_IA.append(freq_exp_IA)
        freq_list_CP.append(freq_exp_CP)

        dxy_list.append(site_dxy)

        depth_list.append(site_depth)

        G_list.append(site_G)

        Fst_list.append(site_Fst)
        
        positions.append(pos)

       
    if called_bases == 10:     #Every 1000 called bases, calculate end position of window and avg. divergence stats for the window.
        #print("Window snp positions: " + str(positions))
        window_start = positions[0]
        window_end = positions[-1]

        window_freq_IA = float(sum(freq_list_IA))/len(freq_list_IA)  #window avg alt allele freq equals avg of all incl. fixed sites
        window_freq_CP = float(sum(freq_list_CP))/len(freq_list_CP)
        freq_diff = window_freq_IA - window_freq_CP

        window_pi_IA = float(sum(pi_list_IA))/len(pi_list_IA)  #window pi is equal to average of site pi across all sites in window, including invariant ones
        window_pi_CP = float(sum(pi_list_CP))/len(pi_list_CP)

        window_dxy = float(sum(dxy_list))/len(dxy_list)

        missing_data = (window_end - window_start) - len(pi_list_IA)

        window_depth = float(sum(depth_list))/len(depth_list)

        #print G_list
        window_G = float(sum(G_list))/len(G_list)
        
        window_Fst = float(sum(Fst_list))/len(Fst_list)

        sys.stdout.write(str(scaff) + "\t" + str(window_start) + "\t" + str(window_end) + "\t" + str(window_freq_CP) +"\t"+ str(window_freq_IA) + "\t" + str(num_snps) +"\t" + str(window_pi_CP) + "\t" + str(window_pi_IA) + "\t" + str(window_dxy) + "\t"+ str(window_G) +"\t"+ str(window_Fst) +"\t"+ str(missing_data) +"\t" + str(window_depth)+ "\n" )

        #Reset window lists to zero
        pi_list_IA=[]
        pi_list_CP=[]
        freq_list_IA=[]
        freq_list_CP=[]
        dxy_list=[]
        depth_list=[]
        G_list=[]
        Fst_list=[]
        positions=[]
        called_bases = 0
        num_snps = 0

#get the last window
if called_bases > 500:
    window_start = positions[0]
    window_end = positions[-1]

    window_freq_IA = float(sum(freq_list_IA))/len(freq_list_IA)  #window avg alt allele freq equals avg of all incl. fixed sites
    window_freq_CP = float(sum(freq_list_CP))/len(freq_list_CP)
    freq_diff = window_freq_IA - window_freq_CP

    window_pi_IA = float(sum(pi_list_IA))/len(pi_list_IA)  #window pi is equal to average of site pi across all sites in window, including invariant ones
    window_pi_CP = float(sum(pi_list_CP))/len(pi_list_CP)

    window_dxy = float(sum(dxy_list))/len(dxy_list)

    missing_data = (window_end - window_start) - len(pi_list_IA)

    window_depth = float(sum(depth_list))/len(depth_list)

    window_G = float(sum(G_list))/len(G_list)
    
    window_Fst = float(sum(Fst_list))/len(Fst_list)

    sys.stdout.write(str(scaff) + "\t" + str(window_start) + "\t" + str(window_end) + "\t" + str(window_freq_CP) +"\t"+ str(window_freq_IA) + "\t" + str(num_snps) + "\t" + str(window_pi_CP) + "\t" + str(window_pi_IA) + "\t" + str(window_dxy) + "\t"+ str(window_G) +"\t"+ str(window_Fst) +"\t"+ str(missing_data) +"\t" + str(window_depth)+ "\n" )

snape_file.close()