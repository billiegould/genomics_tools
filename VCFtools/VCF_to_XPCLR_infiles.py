# -*- coding: utf-8 -*-
"""
Created on Thu Jul 10 14:10:49 2014

@author: Billie
"""

# script will generate XP-CLR input files (3) from a SINGLE CHR VCF: genotype file for early group, one for late group, an a snp info file (mapfile).
#Input one vcf PER CHROMOSOME, BIALLELIC ONLY
#useage: script VCR.ChrX.recode.vcf Chrnumber pop1_lines.txt pop2_lines.txt

import sys
import vcf

filename1="Chr"+str(sys.argv[2])+"_pop1.txt"
filename2="Chr"+str(sys.argv[2])+"_pop2.txt"
mapfilename="Chr"+str(sys.argv[2])+"_Mapfile.txt"

genofile1=open(filename1, "w")
genofile2=open(filename2, "w")
mapfile=open(mapfilename, "w")

pop1lines = open(sys.argv[3], "r")
pop1samples = []
for line in pop1lines:
    pop1samples.append(line.split()[0])
pop1lines.close()

pop2lines = open(sys.argv[4], "r")
pop2samples = []
for line in pop2lines:
    pop2samples.append(line.split()[0])
pop2lines.close()

VCF=vcf.Reader(open(sys.argv[1], "r"))
counter = 0

for line in VCF:
    counter += 1
    if counter % 10000 == 0:
        print ("processing snp number " + str(counter))
    snpname = str(line.CHROM) + "_" + str(line.POS)
    mapfile.write(str(snpname) +"\t"+ line.CHROM +"\t"+ str(line.POS) +"\t"+ line.REF +"\t"+ line.ALT[0] +"\n")

    for s in pop1samples:
        genofile1.write(line.genotype(s)['GT'][0] + " " + line.genotype(s)['GT'][2] + " ")
    genofile1.write("\n")

    for t in pop2samples:
        genofile2.write(line.genotype(t)['GT'][0] + " " + line.genotype(t)['GT'][2] + " ")
    genofile2.write("\n")

print "Output files: " + str(filename1) + "::" + str(filename2) + "::" + str(mapfilename)
genofile1.close()
genofile2.close()
mapfile.close()
print "Complete. . . . Remember to transform (.) to (9) in the genotype files!!"
