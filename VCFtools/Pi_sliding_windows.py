# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>
#
#This script will calculate pi window values for overlapping (sliding) windows across each chromosome in a VCF. Output is one file containing window values
# for each chromosome. Window size is specified as number of called bases (number of VCF lines).

#this pi calculation assumes non-snp sites are all homozyg. ref. some may actually be missing data so this represents a lower bound on window diversity.


# Useage: Pi_sliding_windows.py window_size step_size

import sys
#sys.path.append("/data/billie.gould/SCRIPTS/PyVCF-master")

import vcf

VCF = sys.argv[1]
windowsize = int(sys.argv[2])
step = int(sys.argv[3])

chrom_prev="start"
pi_list=[]
sites=[]

reader = vcf.Reader(open(sys.argv[1], "r"))

for line in reader:
    if line.CHROM != chrom_prev:     #check for a new chromosome
        print chrom_prev + " complete."
        filename = line.CHROM + ".pi_windows"
        output = open(filename, "w")
        output.write('%s\t%s\t%s\t%s\t%s\n' % ("CHR","WIN_START","WIN_END","WIN_MID","PI_WIN"))
        chrom_prev = line.CHROM
        pi_list = []
        sites = []

    pi=line.nucl_diversity
    pi_list.append(pi)
    sites.append(line.POS)

    if len(pi_list) == windowsize:  # calculate a window average
        #print pi_list
        pi_win = sum(pi_list)/(sites[-1]-sites[0])   #this pi calculation assumes non-snp sites are all homozyg. ref. some may actually be missing data so this represents a lower bound on window diversity.
        #print pi_win
        win_mid = (sites[0]+sites[-1])/2
        output.write('%s\t%s\t%s\t%s\t%s\n' % (line.CHROM, sites[0], sites[-1], win_mid, pi_win))
        pi_list = pi_list[step:]    #slide the window by interval
        sites = sites[step:]
