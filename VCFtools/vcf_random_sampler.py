# This script will sample a specified number of sites at random from a vcf and write them to another vcf.

#Useage: python vcf_random_sampler.py infile.vcf #_lines_to_sample > out.vcf

import sys
import random

vcf = open(sys.argv[1], "r")

no_sites_to_sample = int(sys.argv[2])

#outfile = open((sys.argv[3]+".vcf"), "w")

for line in vcf:
    if "#" in line:
        sys.stdout.write(line)
    else:
        break
vcf.close()

vcf = open(sys.argv[1], "r")
sites = random.sample(vcf.readlines(), no_sites_to_sample)

for s in sites:
    sys.stdout.write(s)

#outfile.close()
vcf.close()
