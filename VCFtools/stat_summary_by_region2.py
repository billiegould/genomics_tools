## This file will take the snp-wise Fst statistic produced by VCFtools and produce an average value for each gene region in a gff. This script is slow but handles overlapping gene regions.
print("** replace negative Fst values in infile with zeros first. SNP file must be sorted. GFF does not need to be sorted.")
##  Writes to StdOut

## python stat_summary_by_region.py out.weir.fst genes.gff > outfile.txt

## input Fst file format:
## CHROM	POS	WEIR_AND_COCKERHAM_FST
##  Chr01	1298	1
##  Chr01	1661	-nan
##  Chr01	1663	1
##  Chr01	1838	-0.144353



import sys

snp_stats = open(sys.argv[1], "r")

snp_dict = {}
for line in snp_stats:
    if "CHROM" in line: continue
    elif line.split()[0] in snp_dict.keys():
        snp_dict[line.split()[0]].append((line.split()[1:3]))   # for each chr create list of coord:fst pairs
    elif line.split()[0] not in snp_dict.keys():
        snp_dict[line.split()[0]] = [(line.split()[1:3])]
snp_stats.close()

sys.stderr.write("SNP stats read in.")

print("GENE" + "\t" + "CHROM" + "\t" + "START" + "\t" + "END" + "\t" + "NO.SNPS" + "\t" + "AVG.FST")

gff = open(sys.argv[2], "r")
prev_chrm = 0
for gene in gff:                    # for each gene in a genes_only.gff
    name = gene.split()[8][3:15]
    chrm = gene.split()[0]
    start = int(gene.split()[3])
    end = int(gene.split()[4])
    region = gene.split()[2]
    gene_fstlist= []						#set gene stat list to zero
    #sys.stderr.write(chrm +"\t"+ str(start) +"\t"+ str(end))

    if chrm != prev_chrm: 					#look up snps on correct chromosome
	sys.stderr.write("Starting Chr " + str(chrm)+ "\n")
        try:
            snplist = snp_dict[chrm]
            #print(snplist[0:6])
        except KeyError:
            sys.stderr.write("Err: Chr "+ str(chrm)+ " no SNPs.\n" )
            prev_chrm = chrm
	    continue

    for item in snplist:					#record snp stats within the gene region of the chr
        if int(item[0]) < start: continue

        if start <= int(item[0]) <= end:
            if item[1] == "-nan": continue
            else:
                gene_fstlist.append(float(item[1]))

        if int(item[0]) > end: break

    prev_chrm = chrm						#prev_Chrm = chrm

    no_snps = len(gene_fstlist)
    if no_snps == 0:
        avg_fst = "no_snps_called"
    else:
        avg_fst = sum(gene_fstlist)/no_snps			# write gene summary stat to output
    print('%s\t%s\t%s\t%s\t%s\t%s\t%s' % (name, chrm, start, end, no_snps, avg_fst, region))

sys.stderr.write("Completed Fst summary.")
gff.close()

