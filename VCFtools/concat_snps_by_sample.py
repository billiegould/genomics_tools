## This script will import a vcf file and create a concatenated sequence of bases for each individual sample, output in Fasta format.
# Writes to StdOut
# The script is designed for SNPs only and does not handle indel data. It is intended to concatenate SNPs for later use in generating phylogenetic trees using raxml

import sys

#sys.path.append("/Users/Billie/Desktop/GENETICS_SOFTWARE/PyVCF-master")
import vcf

# import vcf adn define list of samples
iupac_dict={'A/A':'A','T/T':'T','C/C':'C','G/G':'G', "./.":'N', 'A/T':'W', 'A/C':'M' ,'A/G':'R' ,'T/C':'Y' ,'T/G':'K' ,'C/G':'S', 'T/A':'W', 'C/A':'M' ,'G/A':'R' ,'C/T':'Y' ,'G/T':'K' ,'G/C':'S'}
reader = vcf.Reader(open(sys.argv[1], "r"))
samples=reader.samples
seq_dict={}

#create an empty sequence list for each sample.as
for i in samples:
    seq_dict[i]=[]

#add bases to each sequence for each site
for line in reader:

    for s in samples:

        if (line.genotype(s).gt_bases) == None:
            base = 'N'
        else:
            base = iupac_dict.get(line.genotype(s).gt_bases)

        sequence = seq_dict[s]
        sequence.append(base)

for item in seq_dict:
    sys.stdout.write( ">" + item + "\n" + ''.join(seq_dict[item]) +"\n")




#join sequence lists and write out into a fasta file.as