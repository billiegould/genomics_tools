# -*- coding: utf-8 -*-
"""
Created on Tue May 27 10:59:51 2014

@author: Billie
"""

import sys
import re

print "this script will tabluste the alt allele frequencies for each line in the input vcf. output includes pos, freq, number of gt's, and foldedness (whether ancestral allele is known)."
print "useage: python VCF_to_SFinput.py input.vcf outfilename"

output=open(sys.argv[2], "w")
output.write("position" + "\t" + "x" + "\t" + "n" + "\t" + "folded" + "\n")
vcf=open(sys.argv[1], "r")

snpnumber=0
for line in vcf:
    if "#" in line:
        continue
    else:
        snpnumber=snpnumber+1
        data=re.split('\t|:', line)
        position=data[1]
        A=data.count("0/0")
        B=data.count("0/1")
        C=data.count("1/1")
        #refalleles= (2*A)+B
        altalleles= (2*C)+B
        x=altalleles
        n=2*(A+B+C)
        output.write(str(position) + "\t" + str(x) + "\t" + str(n) + "\t" + "0" + "\n")
    

print "Processed " + str(snpnumber) + " variants"

vcf.close()
output.close()

