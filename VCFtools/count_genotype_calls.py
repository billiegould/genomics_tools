
## this script will input a VCF and tally the number of substitutions between each genotype and the reference genome as well as the number of missing genotype calls.
## These data are then used to calculate divergence rates and output in a table.

import sys
import vcf

#read in file
reader = vcf.Reader(open(sys.argv[1], "r"))
samples=reader.samples

#make dictionary of sample names and alt allele sums
aaf_dict={}
md_dict={}
called_sites_dict={}

for s in samples:
    aaf_dict[s]=0
    md_dict[s]=0
    called_sites_dict[s]=0

#make dictionary of sample names and missing data sums
#make dictionary of sample names and number called sites

#for each line, for each sample add the data to the dictionaries
for record in reader:
    for s in samples:

        if record.genotype(s) == "0/0":
            called_sites_dict[s] += 1

        if record.genotype(s) == "0/1":
            called_sites_dict[s] += 1
            aaf_dict[s] += 1

        if record.genotype(s) == "1/1":
            called_sites_dict[s] += 1
            aaf_dict[s] += 2

        if record.genotype(s) == "./."
            md_dict[s] += 1

for s in samples:
    D_high = aaf_dict[s]/(2* (md_dict[s] + called_sites_dict[s]))  # denominator = 2 * total number non-missing sites : only includes variable sites in calculation
    D_low = aaf_dict[s]/(2 * (492719991 - md_dict[s]))          # denominator = 2 * total bases in genome assembly : assumes all non-called sites are homz ref

#for each sample in each dictionary calculate D low and D high and write to table.