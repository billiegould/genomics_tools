# This script will take a list of samples per population and a VCF and output global FST averaged across sliding windows.
# The vcf must be zipped with bgzip and indexed with tabix

# input sample list format as sample_name, pop. delimited

import sys
import vcf
from Div_stats_module import snpFst

# create population dictionary:

populations = open(sys.argv[2], "r")

sample_dict = {}
for line in populations:
    pop = line.split()[1]
    sample = line.split()[0]
    if pop in sample_dict.keys():
        sample_dict[pop].append(sample)
    else:
        sample_dict[pop] = [sample]

populations.close()
sys.stderr.write("Samples read in. \n")

vcf = vcf.Reader(open(sys.argv[1], "r"))

for pop in sample_dict.keys():                  # check that all samples are in the VCF
    for sample in sample_dict[pop]:
        if sample in vcf.samples:
            continue
        elif sample not in vcf.samples:
            sys.stderr.write("ERROR: sample " + str(sample) + " not in VCF. \n")

for sample in vcf.samples:                   #check for extra samples in the VCF
    status = "missing"
    for pop in sample_dict.keys():
        if sample in sample_dict[pop]:
            status = "found"
    if status == "missing":
        sys.stderr.write("WARNING: Extra sample: " + str(sample) + "  present in the VCF. \n")

sys.stdout.write("%s\t%s\t%s\t%s\t%s\n" % ("Chr","win_start", "win_end", "num.snps", "Fst") )

for scaff in vcf.contigs:
    sys.stderr.write("Processing scaffold: " + str(scaff) +"\n")
    win_start = 0
    fsts_w1 = []
    fsts_w2 =[]

    try:
	section = vcf.fetch(scaff)
    except ValueError:
	sys.stderr.write("WARNING: There is no scaffold " +str(scaff)+ " in the VCF.\n")
	continue

    for line in section:
	status = "unplaced"
	fst = snpFst(line, sample_dict)
        if fst != "NA":
            while status == "unplaced":		# only move to next snp once present snp is placed in a window
	  
	        if win_start < int(line.POS) <= int(win_start + 1500):      # snp in left half of window
                    fsts_w1.append(fst)
		    status = "placed"

	        elif (win_start + 1500) < int(line.POS) <= int(win_start + 3000):	#snp in right half of window
		    fsts_w1.append(fst)
		    fsts_w2.append(fst)
		    status = "placed"

                elif int(line.POS) > int(win_start + 3000):			# snp beyond window
            	    if len(fsts_w1) != 0:
                        window_fst = sum(fsts_w1)/len(fsts_w1)
            	    else:
                        window_fst = "NA"
	            #print("FSTs: " +str(fsts_w1) +"\n")

                    sys.stdout.write("%s\t%s\t%s\t%s\t%s\n" % (line.CHROM, win_start, str(win_start + 3000), len(fsts_w1), window_fst))
                    win_start += 1500                # slide the window start
	            fsts_w1 = fsts_w2
		    fsts_w2 =[]

sys.stderr.write("Complete")
