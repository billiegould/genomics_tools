# -*- coding: utf-8 -*-
"""
Created on Mon Jun  2 17:18:46 2014

@author: Billie
"""

##this script will extract variants from a vcf depending on input keyword


import sys
import re

if (sys.argv[1] == ""):
    print "Useage: python script input_vcf outfile_name ANNOTATION_TERM"
else:
    print "Processing . . ."
    output=open(sys.argv[2], "w")
    data=open(sys.argv[1], "r")

    for line in data:
        if "#" in line:
            output.write(line)
        else:
            snp=re.split('\t|,', line)
            if "SYNONYMOUS_CODING" in snp]
	    	output.write(line)                 
            else:
                continue

output.close()
data.close()
print "COMPLETED"
