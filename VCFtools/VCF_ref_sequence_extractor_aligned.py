# -*- coding: utf-8 -*-
"""
Created on Fri Nov 21 15:27:32 2014

@author: Billie
"""
## this script should take a list of gene regions and write out the reference sequence based on reference alleles listed in a vcf adjusting length where there are insertions in the alt allele
#chromosome specific gene list: genename chrom genestart geneend
#imput VCF does NOT need to contain every base on the chromosome, be compressed using bgzip, and indexed with tabix
#useage: script inputVCF.gz genelist
# script will NOT insert ?s for missing bases. use with snps only vcf.** for insertions and deletions, script inserts gaps "-" equivalent to the allele with the longest length. output sequences are 'aligned' to the reference and ready for distance calculations/tree building. 

import sys
#sys.path.append("/Users/Billie/Desktop/GENETICS_SOFTWARE/PyVCF-master")
sys.path.append("/data/billie.gould/SCRIPTS/PyVCF-master/")
import vcf
import pysam

#iupac_dict={'A/A':'A','T/T':'T','C/C':'C','G/G':'G', "./.":'N', 'A/T':'W', 'A/C':'M' ,'A/G':'R' ,'T/C':'Y' ,'T/G':'K' ,'C/G':'S', 'T/A':'W', 'C/A':'M' ,'G/A':'R' ,'C/T':'Y' ,'G/T':'K' ,'G/C':'S'}

#for testing: reader = vcf.Reader(open("test_individ_vcf.txt", "r"))

def construct(section, genestart, geneend, refseqbases):
    prev_pos=int(genestart)-1
    dup_pos=0
    lines_processed=0
    skips=0
    for line in section:
        lines_processed=lines_processed+1
        if int(line.POS)%100 == 0: #if the number of lines processed is evenly divisible by 100 the print the line you are at
            print str(line)
        if genestart <= line.POS <= geneend:  # if the line position is within the gene , 
            if line.POS == prev_pos: #if a duplicate position/variant exists use the last one
                dup_pos=dup_pos +1
                prev_pos=line.POS
                continue #read in next item/line in the chrom list
            elif line.POS > prev_pos:
                skips= skips + (int(line.POS) - prev_pos - 1)
                               #then add the refbase info for that line
                if line.ALT[0] == None:
		    refseqbases.append(str(line.REF)) 
		elif len(line.ALT[0]) <= len(line.REF):
                    refseqbases.append(str(line.REF))
                elif len(line.ALT[0]) > len(line.REF):          #for deletions
                       #print "deletion detected"
		    newRefbase = str(line.REF) + ("-" * (len(line.ALT[0]) - len(line.REF)))
                       #print "alt: " + str(line.ALT[0]) + "  ref: " + str(newRefbase)
                    refseqbases.append(str(newRefbase))
                prev_pos=line.POS
    print "duplicate poitions: " +str(dup_pos)
    print "missing positions: " +str(skips)
    print "lines processed: " +str(lines_processed)

#######################################
prev_pos=0

reader = vcf.Reader(open(sys.argv[1], "r"))
samples=reader.samples

genelist=open(sys.argv[2], "r")

for gene in genelist:           #chromosome specific gene list: genename chrom genestart geneend
    genename = gene.split()[0]      #create a dictiosamples=reader.samplesnary of empty sequence lists for each sample for the gene
    print "Processing: " + str(genename)
    chrom=gene.split()[1]
    genestart=int(gene.split()[2])
    geneend=int(gene.split()[3])
    refseqbases=[]

    section = reader.fetch(chrom, (genestart-1), geneend)       #retrieve vcflines within the gene
    construct(section, genestart,geneend,refseqbases)  #use this function to construct the allele set

    outfile= (str(genename) + "_ref_seq.fa")
    output=open(outfile, "w")
    polymer=''.join(refseqbases)
    output.write(">" + str(genename) + "_ref_seq_TAIR10" + "\n" + str(polymer) + "\n")
    output.close()
    print "Gene extracted: " + str(genename)

genelist.close()

