# -*- coding: utf-8 -*-
"""
Created on Fri Nov 21 15:27:32 2014

@author: Billie
"""
## this script should take a list of gene regions and write out the reference sequence based on reference alleles listed in a vcf. Does NOT insert gaps where there are insertions in the VCF population.
#chromosome specific gene list: genename chrom genestart geneend
#VCF should be compressed using bgzip, and indexed with tabix
#useage: script inputVCF.gz genelist

import sys
#sys.path.append("/Users/Billie/Desktop/GENETICS_SOFTWARE/PyVCF-master")
sys.path.append("/data/billie.gould/SCRIPTS/PyVCF-master/")
import vcf
import pysam

#iupac_dict={'A/A':'A','T/T':'T','C/C':'C','G/G':'G', "./.":'N', 'A/T':'W', 'A/C':'M' ,'A/G':'R' ,'T/C':'Y' ,'T/G':'K' ,'C/G':'S', 'T/A':'W', 'C/A':'M' ,'G/A':'R' ,'C/T':'Y' ,'G/T':'K' ,'G/C':'S'}

#for testing: reader = vcf.Reader(open("test_individ_vcf.txt", "r"))

def construct(section, genestart, geneend, refseqbases):
    prev_pos=int(genestart)-1
    dup_pos=0
    lines_processed=0
    skips=0
    for line in section:
        lines_processed=lines_processed+1
        if int(line.POS)%100 == 0: #if the number of lines processed is evenly divisible by 100 the print the line you are at
        # print str(line)
        if genestart <= line.POS <= geneend:  # if the line position is within the gene , 
            if line.POS == prev_pos: #if a duplicate position/variant exists use the last one
                dup_pos=dup_pos +1
                prev_pos=line.POS
                continue #read in next item/line in the chrom list
            elif line.POS > prev_pos:  
                skips= skips + (int(line.POS) - prev_pos - 1)
                               #then add the refbase info for that line
                refseqbases.append(str(line.REF))
                    #sequence.append((skips * '?'))  ## use this line to insert ? where bases are missing from the vcf file
                    #print "sample: " +str(s)+ " GT: " + str(line.genotype(s).gt_bases) 
                    #if (line.genotype(s).gt_bases) == None or line.genotype(s).data[2] == None :
			#sequence.append("N")
		        #elif int(line.genotype(s).data[2]) >= dp_cutoff: #if there is genotype data and DP is sufficient, add the base/genotype to the sequence
                        #base=iupac_dict.get(line.genotype(s).gt_bases)   #iupac dictionary includes N for missing data
                        #print "Base: " +str(base)
                        #f base == None:
                        #    sequence.append(line.genotype(s).gt_bases.split("/")[1])  #insert the second(alt in case f hets) allele at the site, could be an insertion or deletion
                        #else:    
                         #   sequence.append(base) #add genotype to the sequence for that sample
                    #else:
                        #sequence.append("N")
                prev_pos=line.POS
    print "duplicate poitions: " +str(dup_pos)
    print "missing positions: " +str(skips)
    print "lines processed: " +str(lines_processed)
#######################################
prev_pos=0

reader = vcf.Reader(open(sys.argv[1], "r"))
samples=reader.samples

genelist=open(sys.argv[2], "r")

for gene in genelist:           #chromosome specific gene list: genename chrom genestart geneend
    genename = gene.split()[0]      #create a dictiosamples=reader.samplesnary of empty sequence lists for each sample for the gene
    print "Processing: " + str(genename)
    chrom=int(gene.split()[1])
    genestart=int(gene.split()[2])
    geneend=int(gene.split()[3])
    refseqbases=[]

    section = reader.fetch(chrom, (genestart-1), geneend)	#retrieve vcflines within the gene
    construct(section, genestart,geneend,refseqbases)
    
    outfile= (str(genename) + "_ref_seq.fa")
    output=open(outfile, "w")
    polymer=''.join(refseqbases)  
    output.write(">" + str(genename) + "_ref_seq" + "\n" + str(polymer) + "\n")
    output.close()
    print "Gene extracted: " + str(genename)
    
genelist.close()

