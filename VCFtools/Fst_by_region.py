## This script will calculate FST from a VCF for a list of gene regions in a GFF.
## Input VCF must be sorted by chr, zipped with bgzip and indexed with tabix
## Genes without any VCF lines are recorded as NA

## Useage: script.py VCF REGIONS POP1 > OUTFILE

import sys
import vcf
import math

############
def geneFst(gene_vcf_lines, fil_samples, hal_samples):  # see cockerham and weir 1984
    
    fst_list=[]

    for snp in gene_vcf_lines:
#        print(snp)
	
	r = 2  # number of populations
	
        nF = 0  # sample size FIL
        npf = 0
        nhet = 0
        for f in fil_samples:
	    call = snp.genotype(f)
            if call.gt_type == 0:
                nF += 2
                npf += 2
            elif call.gt_type == 1:
                nhet += 1
                nF += 2
		npf += 1
            elif call.gt_type == 2:
                nF += 2
#            p_F = float(npf)/float(nF)  # REF allele freq FIL
#            nhet_F = nhet / (nF / 2.0)  # FIL het. frequency
#	print("np:"+str(np) + "\t"+ "nF:"+str(nF)+ "\t"+ "p_F: " + str(p_F) + "\n")
#	print("nhet_F: " + str(nhet_F) + "\n")

        nH = 0  # sample size HAL
        nph = 0
        nhet = 0
        for h in hal_samples:
	    call = snp.genotype(h)
            if call.gt_type == 0:
                nH += 2
                nph += 2
            elif call.gt_type == 1:
                nhet += 1
                nH += 2
		nph += 1
            elif call.gt_type == 2:
                nH += 2
#            p_H = float(nph) / float(nH)  # REF allele freq in HAL
#            nhet_H = nhet / (nH / 2.0)  # HAL het. frequency
#	print("np_H: " + str(p_H) + "\n")
#        print("nhet_H: " + str(nhet_H) + "\n")

        if nH >= 10 and nF >= 10:  # calculate FST for sites with at least 5 fil and 5 hal called.
            
	    p_F = float(npf)/float(nF)  # REF allele freq FIL
            nhet_F = nhet / (nF / 2.0)  # FIL het. frequency
	    p_H = float(nph) / float(nH)  # REF allele freq in HAL
            nhet_H = nhet / (nH / 2.0)  # HAL het. frequency

	    nbar = (nH + nF) / 2.0  # average sample size in chr
#	    print("nbar: " + str(nbar) +"\n")
	
            nc = ((r * nbar) - ( ( (nF * nF) / (r * nbar) ) +  ((nH * nH) / (r * nbar)) )) / (r - 1)  # coeff of variation of sample sizes
#	    print("nc: " + str(nc) +"\n")

            pbar = ((nF * p_F) + (nH * p_H)) / (r * nbar)  # avg pop frequency of the REF allele
#	    print("pbar: " + str(pbar) +"\n")

            svar = ((nF * math.pow((p_F - pbar), 2)) + (nH * math.pow((p_H - pbar), 2))) / ((r - 1) * nbar)  # the variance of REF freqs
#	    print("svar: " + str(svar) +"\n")

            het_bar = ((nF * nhet_F) + (nH * nhet_H)) / (r * nbar)
#	    print("het_bar: " + str(het_bar) +"\n")
            
	    A = (nbar / nc) * ((svar - (1 / (nbar - 1)) * ((pbar * (1 - pbar)) - (((r - 1) / r) * svar) - (0.25 * het_bar))))

            B = (nbar / (nbar - 1)) * ((pbar * (1 - pbar)) - (((r - 1) / r) * svar) - ((((2 * nbar) - 1) / (4 * nbar)) * het_bar))

            C = 0.5 * het_bar

            FST = A / (A + B + C)
#	    print("FST= " + str(A) +" / ("+ str(A) +" + "+ str(B) +" + "+ str(C) +" ) \n")

            fst_list.append(FST)

#	    print(str(snp.POS) + "\t" + str(p_F - p_H) + "\t" + str(FST))

    if len(fst_list) == 0:
        region_FST = "NA"
        no_snps = 0
    else:
        region_FST = math.fsum(fst_list) / len(fst_list)
        no_snps = len(fst_list)

#    print("FstList: " + str(fst_list))
    return (no_snps, region_FST)


#####################

pop1 = open(sys.argv[3], "r")
fil_samples=[]
for line in pop1:
    fil_samples.append(line.strip())
#print("fil_Samples: " + str(fil_samples) +"\n")

reader = vcf.Reader(open(sys.argv[1], "r"))
hal_samples = [s for s in reader.samples if s not in fil_samples]
#print("hal_Samples: " + str(hal_samples) +"\n")

genelist = open(sys.argv[2], "r")
sys.stdout.write("GENE \t Chr \t Start \t End \t No.Sites \t FST \t region \n")

for gene in genelist:
    genename = gene.split()[8][3:15]
    # print "Processing: " + str(genename)
    chrom = gene.split()[0]
    genestart = int(gene.split()[3])
    geneend = int(gene.split()[4])
    region = gene.split()[2]

    try:
        gene_vcf_lines = reader.fetch(chrom, (genestart - 1), geneend)  # retrieve vcflines within the gene
    except ValueError:
        sys.stdout.write(str(genename) + "\t" + str(chrom) + "\t" + str(genestart) + "\t" + str(geneend) + "\t NA \t NA \n")
    else:
        (no_snps, region_FST) = geneFst(gene_vcf_lines, fil_samples, hal_samples)

        sys.stdout.write(str(genename) + "\t" + str(chrom) + "\t" + str(genestart) + "\t" + str(geneend) + "\t" + str(no_snps) + "\t" + str(region_FST) + "\t" + str(region) + "\n")

sys.stderr.write("Complete")

