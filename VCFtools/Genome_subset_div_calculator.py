# Useage: script.py infile interval_list > outfile

#print NOTE: Run this script on one snape file per chromosome (snps only).

# before running, store interval file as pickle object, a stored python dictionary

import sys
import Div_stats_module
import pickle
import math


sys.stdout.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % ("scaff","win.start","win.end","freqCP","freqIA","freq_diff","piCP","piIA","Dxy_win","win_G","win_Fst","missing_data","win_depth"))

snps = open(sys.argv[1], "r")
scaffold = snps.readline().split()[0]
snps.close()

stored_intervals = open((sys.argv[2] + ".pkl"), 'rb')   #use the full path to pickled file here
interval_dict = pickle.load(stored_intervals)       #a stored dictionary of intervals per scaffold
regions = sorted(interval_dict[scaffold])


snps = open(sys.argv[1], "r")

#initialize lists
pi_list_IA=[]
pi_list_CP=[]
freq_list_IA=[]
freq_list_CP=[]
dxy_list=[]
depth_list=[]
G_list=[]
Fst_list=[]
positions=[]
called_bases = 0

for interval in regions:
    reg_start = int(interval[0])
    reg_end = int(interval[1])
    #print interval
    for line in snps:
        pos = int(line.split()[1])

        if pos < reg_start:
            #print "skip" + str(pos)
            continue

        elif pos > reg_end:
            #print "skip"+ str(pos)
            break

        elif reg_start <= pos <= reg_end:
            #print "calc." + str(pos)
            stats = Div_stats_module.calc_stats(line)
            #print stats

            if stats != None:
                called_bases += 1
                (pos, freq_exp_IA, freq_exp_CP, site_pi_IA, site_pi_CP, site_depth, site_Fst, site_G, site_dxy) = stats

                #write statistics to window lists
                pi_list_IA.append(site_pi_IA)
                pi_list_CP.append(site_pi_CP)

                freq_list_IA.append(freq_exp_IA)
                freq_list_CP.append(freq_exp_CP)

                dxy_list.append(site_dxy)

                depth_list.append(site_depth)

                G_list.append(site_G)

                Fst_list.append(site_Fst)

                positions.append(pos)

            #calculate window values
            if called_bases == 1000:           ## Set window size here
                #print("Window snp positions: " + str(positions))
                window_start = positions[0]
                window_end = positions[-1]
                window_freq_IA = float(sum(freq_list_IA))/len(freq_list_IA)  #window avg alt allele freq equals avg of all incl. fixed sites
                window_freq_CP = float(sum(freq_list_CP))/len(freq_list_CP)
                freq_diff = window_freq_IA - window_freq_CP

                window_pi_IA = float(sum(pi_list_IA))/len(pi_list_IA)  #window pi is equal to average of site pi across all sites in window, including invariant ones
                window_pi_CP = float(sum(pi_list_CP))/len(pi_list_CP)

                window_dxy = float(sum(dxy_list))/len(dxy_list)

                missing_data = (window_end - window_start) - len(pi_list_IA)

                window_depth = float(sum(depth_list))/len(depth_list)

                window_G = float(sum(G_list))/len(G_list)

                window_Fst = float(sum(Fst_list))/len(Fst_list)

                sys.stdout.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (scaffold, window_start, window_end, window_freq_CP, window_freq_IA, freq_diff, window_pi_CP, window_pi_IA, window_dxy, window_G, window_Fst, missing_data, window_depth))

                #Reset window lists to zero
                pi_list_IA=[]
                pi_list_CP=[]
                freq_list_IA=[]
                freq_list_CP=[]
                dxy_list=[]
                depth_list=[]
                G_list=[]
                Fst_list=[]
                positions=[]
                called_bases = 0

#get the last window after all intervals are complete
if called_bases > 500:
    window_start = positions[0]
    window_end = positions[-1]

    window_freq_IA = float(sum(freq_list_IA))/len(freq_list_IA)  #window avg alt allele freq equals avg of all incl. fixed sites
    window_freq_CP = float(sum(freq_list_CP))/len(freq_list_CP)
    freq_diff = window_freq_IA - window_freq_CP

    window_pi_IA = float(sum(pi_list_IA))/len(pi_list_IA)  #window pi is equal to average of site pi across all sites in window, including invariant ones
    window_pi_CP = float(sum(pi_list_CP))/len(pi_list_CP)

    window_dxy = float(sum(dxy_list))/len(dxy_list)

    missing_data = (window_end - window_start) - len(pi_list_IA)

    window_depth = float(sum(depth_list))/len(depth_list)

    window_G = float(sum(G_list))/len(G_list)

    window_Fst = float(sum(Fst_list))/len(Fst_list)

    sys.stdout.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (scaffold, window_start, window_end, window_freq_CP, window_freq_IA, freq_diff, window_pi_CP, window_pi_IA, window_dxy, window_G, window_Fst, missing_data, window_depth))

snps.close()


