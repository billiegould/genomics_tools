## This script will calculate population genetic statistics from a VCF for a list of gene regions in a GFF.
## Input VCF must be sorted by chr, zipped with bgzip and indexed with tabix
## Genes without any VCF lines are recorded as NA

import sys
import vcf

#######################################

def genePi(gene_vcf_lines):
    pi_list = []
    for snp in gene_vcf_lines:
        pi_list.append(snp.nucl_diversity)

    if len(pi_list)==0:
        gene_pi=0
        no_snps=0
    else:
        gene_pi = sum(pi_list)/len(pi_list)
        no_snps = len(pi_list)

    return(no_snps, gene_pi)

#######################################

reader = vcf.Reader(open(sys.argv[1], "r"))
samples=reader.samples

genelist=open(sys.argv[2], "r")
sys.stdout.write("GENE \t Chr \t Start \t End \t No.Sites \t PI \n")

for gene in genelist:
    genename = gene.split()[8][3:15]
    #print "Processing: " + str(genename)
    chrom=gene.split()[0]
    genestart=int(gene.split()[3])
    geneend=int(gene.split()[4])

    try:
        gene_vcf_lines = reader.fetch(chrom, (genestart -1), geneend)       #retrieve vcflines within the gene
    except ValueError:
        sys.stdout.write(str(genename) + "\t" + str(chrom) + "\t" + str(genestart) + "\t" + str(geneend) + "\t NA \t NA \n")
    else:
    	(no_snps, gene_pi) = genePi(gene_vcf_lines)

        sys.stdout.write(str(genename) + "\t" + str(chrom) + "\t" + str(genestart) + "\t" + str(geneend) + "\t" + str(no_snps) + "\
t" + str(gene_pi) +"\n")

sys.stderr.write("Complete")

