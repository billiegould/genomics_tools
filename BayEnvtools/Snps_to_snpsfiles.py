# -*- coding: utf-8 -*-
"""
Created on Mon May 12 13:42:20 2014

@author: Billie
"""

#this will make each line in the BayEnv SNPSFILE into a SNPFILE; file input fields must be : #snpnumber, Chr, Pos, earlyfreq, latefreq, with one line for each allele at each psotiions.

import sys

#a la Robert
data=open(sys.argv[1], "r")
prevLine = ""
i=0
for line in data:
    if "#" in line:
        continue
    else:
        line = line.rstrip("\n")
        line = line + "\t\n"
        if prevLine == "":
            prevLine = line
        else:
            i=i+1
            filename= "snpfile_" + str(i) + "_" + str(line.split()[1]) + "_" + str(line.split()[2]) + ".txt"
            snpfile=open(filename, "w")
            snpfile.write(prevLine.split()[3] + "\t" +  prevLine.split()[4] + "\n" + line.split()[3] + "\t" + line.split()[4])
            snpfile.close()
            prevLine = ""
print "complete: processed " + str(i) + " snps. Hooray!"


