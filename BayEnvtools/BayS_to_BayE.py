# -*- coding: utf-8 -*-
"""
Created on Fri May  9 16:11:04 2014

@author: Billie
"""

#this script will turn a BayScan file into a BayEnv2 file

print "use R to replace blanks with zero allele frequencies in the input file FIRST: useage: inputfile outputfilename"

import sys

output=open(sys.argv[2], "w")

counter=0
data=open(sys.argv[1], "r")
for line in data:
    counter=counter+1    
    snp=line.split(",")
    output.write(snp[3]+"\n")
    output.write(snp[4]+"\n")

data.close()
output.close()
print "complete: " + str(counter) + " SNPs processed"