# This script will take a list of allele best blast hits, output the gene, hit annotation, inversion status, and reference coordinates in the genome.

# Useage : script.py blastoutput.txt CR.fa ref.gff > annotation.txt  *** Input files must be sorted ***

import sys
import re

# read in gff coordinate annotations as dictionary and annotate inversion genes

# GFF file format

###gff-version 3
##annot-version v2.0
#scaffold_1	phytozomev10	gene	16375	23539	.	-	.	ID=Migut.A00001.v2.0;Name=Migut.A00001;ancestorIdentifier=mgv1a001726m.g.IM62.v1.1
#scaffold_1	phytozomev10	mRNA	16375	23539	.	-	.	ID=Migut.A00001.1.v2.0;Name=Migut.A00001.1;pacid=28939341;longest=1;Parent=Migut.A00001.v2.0

sys.stderr.write("Input files must be sorted. \n")

gff = open(sys.argv[3], "r")
D_gff = {}

for line in gff:
    if "#" not in line and "gene" in line:
        name = (line.split()[8])[3:15]
        chr = line.split()[0]
        start = (line.split()[3])
        end = (line.split()[4])
        strand = line.split()[6]

        if chr == "scaffold_5":              #inv 5 scaffolds
            left=(11466582,11781205,12467777,13082240,13367321,14418230,15044886,16114744,16561107,24038195)
            right=(11646589,12457777,12605828,13357321,13932160,14901369,15663194,16283041,18317358,24294111)
            for i in range(0,10):
                if int(left[i]) < int(start) < int(right[i]):
                    inversion = "I5"

        elif chr == "scaffold_8" and (888603 < int(start) < 7594555):    # scaffold 8 from 888603 to 7594555
            inversion = "I8"

        else:
            inversion = "NA"

        D_gff[name]=[chr, start, end, strand, inversion]
gff.close()

# blast output format: qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp

prev_pair = "pair1"
pair_matches = []
pair_evals = []
pairs_w_matches = []

blast_data = open(sys.argv[1], "r")
sys.stdout.write("## PAIR \t MATCH \t EVAL \t CHR \t START \t END \t STR \t INV \n")
for line in blast_data:
    if ("#" not in line):
        pair = re.split("_", line.split()[0])[0]
        match = line.split()[1][0:12]
        eval = float(line.split()[10])

        if pair == prev_pair:                  # if the present gene matches the previous gene, add dataline to list
            pair_matches.append(match)
            pair_evals.append(eval)
	    prev_pair = pair
        elif pair != prev_pair:
            if len(pair_matches) > 1:				# process the previous list of pair matches
                index = pair_evals.index(min(pair_evals))   # Choose the best match. Note, this returns the first min value. if two matches have the same eval, the first is returned.
            else:                                           # if only one allele has a match and the other doesnt, use that match
                index=0
            best_match = pair_matches[index]
	    match_data = "\t".join(D_gff[best_match])
	    pairs_w_matches.append(pair)
            sys.stdout.write(prev_pair +"\t"+ best_match +"\t"+ str(eval) +"\t"+ match_data +"\n")

	    pair_matches=[match]		# reset the lists for the current transcript
	    pair_evals=[eval]
	    prev_pair = pair

# get the last pair, pair25290
best_match = "Migut.C01115"
eval = 0.0
match_data = "\t".join(D_gff[best_match])
sys.stdout.write("pair25290" +"\t"+ best_match +"\t"+ str(eval) +"\t"+ match_data +"\n")

blast_data.close()

CR = open(sys.argv[2])          # Add lines to output that correspond to transcripts without blast matches.
prev_pair = "pair1"
counter = 0

#print(pairs_w_matches[0:4])

for line in CR:
    if ">" in line:
        pair = str(re.split("_", line)[0][1:])
#	print(pair)
        if (pair != prev_pair) and (pair not in pairs_w_matches):
            sys.stdout.write(pair + "\t -- \t -- \t -- \t -- \t -- \t -- \t -- \n")
            counter += 1
#	    print("Missing!")
        prev_pair = pair
CR.close()

sys.stderr.write( "Annotation complete. There were " + str(counter) + " unmatched transcripts.")

sys.stderr.write( "REMEMBER TO SORT THE OUTPUT FILE BY TRANSCRIPT \n")

