# -*- coding: utf-8 -*-
"""
Created on Wed Jul  2 11:25:39 2014

@author: Billie
"""
###replace "chr" in gff with number nly!!!!!
# this script will find the genes overlapping a list of sliding windows.. . replace chr1 etc in GFF with '1' "
#useage: script input_windows_file gff_file . .. files must be sorted, there must be at least one window per chromosome"

import sys

#create a dictionary with a list of sliding windows per chromosomes
chr1listSTR=[]
chr2listSTR=[]
chr3listSTR=[]
chr4listSTR=[]
chr5listSTR=[]
windowfile=open(sys.argv[1], "r")
for line in windowfile:
    if "CHR" in line.split():
        continue
    elif int(line.split()[0]) == 1:
        chr1listSTR.append((line.split()[4:6]))
    elif int(line.split()[0]) == 2:
        chr2listSTR.append((line.split()[4:6]))
    elif int(line.split()[0]) == 3:
        chr3listSTR.append((line.split()[4:6]))
    elif int(line.split()[0]) == 4:
        chr4listSTR.append((line.split()[4:6]))
    elif int(line.split()[0]) == 5:
        chr5listSTR.append((line.split()[4:6]))

chr1listNUM=[map(float, x) for x in chr1listSTR]
chr2listNUM=[map(float, x) for x in chr2listSTR]
chr3listNUM=[map(float, x) for x in chr3listSTR]
chr4listNUM=[map(float, x) for x in chr4listSTR]
chr5listNUM=[map(float, x) for x in chr5listSTR]

windict = {}
windict[1]=chr1listNUM
windict[2]=chr2listNUM
windict[3]=chr3listNUM
windict[4]=chr4listNUM
windict[5]=chr5listNUM
print "window dictionary constructed, starting gene comparisons . . ."
windowfile.close()

#for each gene in the GFF, compare start and stop to windowdictionary list for that chromosome
output=open("output.txt", "w")
output.write("GENE" + "\t" + "CHR" + "\t" + "OVLP.WIN.START" + "\n")
genefile=open(sys.argv[2], "r")
for line in genefile:
    gene=line.split()
    genechr=int(gene[0])
    #genestart=int(gene[1])
    genestart=int(gene[3])
    #geneend = int(gene[2])
    geneend=int(gene[4])
    genename=gene[8]
    #genename=genestart
    windowlist=windict[genechr]
#for each gene, see if it overlaps any windows, if not read in next gene and repeat
    for window in windowlist:
        windowstart=int(window[0])
        windowend=int(window[1])
        if geneend < windowstart or genestart > windowend:
            continue
            #print "gene " + str(genename) + " non-overlapping with window " + str(windowstart)            
        elif genestart <= windowstart <= geneend:
            output.write(str(genename) + "\t" + str(genechr) + "\t" + str(windowstart) + "\n")
        elif genestart <= windowend <= geneend:
            output.write(str(genename) + "\t" + str(genechr) + "\t" + str(windowstart) + "\n")
        elif windowstart <= genestart and windowend >= geneend:
            output.write(str(genename) + "\t" + str(genechr) + "\t" + str(windowstart) + "\n")
output.close()
genefile.close()
print "Done. printed to 'output.txt' "
