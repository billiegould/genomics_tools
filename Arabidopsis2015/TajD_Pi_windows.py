# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>



import sys
#sys.path.append("/Users/Billie/Desktop/PyVCF-master")
sys.path.append("/data/billie.gould/SCRIPTS/PyVCF-master/")

print "NOTE: Run this window TajD script on one vcf per chromosome. Useage: VCF windowsize  chrom  outfile  avg_indiv_depthcutoff dwnsample_to_n_indv"
print "this script excludes by default non-bialleleic sites, indels, and sites with more than 50% missing genotype calls" 

import vcf
import random
import math

inputVCF=sys.argv[1]
windowsize=float(sys.argv[2])
chrom=int(sys.argv[3])
outfile=sys.argv[4]
dpcutoff=int(sys.argv[5])
n=int(sys.argv[6])*2


#Chrm_lengths={1:30427671, 2:19698289, 3:23459830, 4:18585056, 5:26975502}
#chrmlength=Chrm_lengths[chrom]

reader = vcf.Reader(open(inputVCF, "r"))
sample_names=reader.samples
#_N=len(sample_names)


#Initialize TajD variables:  
#chromsampSize = _N*2  
a1 = 0
a2 = 0
for i in range(1, n): #sum is for up to N-1,since python does not include the last item in the range, we're covered here.
    a1 += 1/float(i)
    a2 += 1/(float(i)*float(i)) # see hartl and clark p. 175
        
b1 = float(n+1)/(3*(n-1))#see Tajima 1989
b2 = float(2*(n*n +n+3))/(9*n*(n-1))

c1 = (b1) - 1/a1
c2 = b2-((float(n+2))/(a1*n))+(a2/(a1*a1))

e1 = c1/a1
e2 = c2/(a1*a1+a2)
        

def myPi(pi_list, midpoint, chrom): 
    window_pi=float(sum(pi_list))/windowsize  # get Pi on a per site basis.add up all individual site 2pq values. divide by number of sites used. 
    print "Window pi equals " + str(window_pi)
    #Pi_output.write( str(chrom) + "\t" + str(midpoint) + "\t" + "\t" + repr(window_avgpi) + "\n")
    return window_pi    #this value is really Tajima's k statistic, not pi itself. #to get pi on a per nucleotide basis, multiply k by Tajima's correction factor (n-1)/n and divide by 10,000 bases in the window
        
def myTajD(pi_list, window_pi, n, a1, e1 ,e2, windowstart, windowend, midpoint, chrom, low_cov_sites, indels, missing_data):
    S=0
    for p in pi_list: #total number of polymorphic sites in the window, S
        if 0 < p < 1:
            S=S+1
    window_length = windowend - windowstart
    if S != 0: #calculate Tajima's D
        theta = float(S)/a1
        print("theta ", theta)
        d = float(window_pi * windowsize) - theta ###ths is Tajima's D, raw. window_pi is readjusted to represent a sum heterozygosity for the window, rather than a per site average. 
        p1 = e1*S
        p2 = e2*S*(S-1)
        denom=float(math.sqrt(p1+p2)) #denominator is the expected variance in d
        print "sqrtvariance= " +str(denom)
        D = d/denom 
        print "TajD= " +str(D)
    else:
        theta = "."
        D = '.'    
    TajD_output.write( str(chrom) + "\t" + str(midpoint) + "\t" + str(window_length)+ "\t" + str(indels)+ "\t"+str(low_cov_sites)+ "\t"+ str(missing_data)+ "\t"+  str(S) + "\t" + repr(window_pi) + "\t" + str(theta) + "\t" + str(D) + "\n")
    

#############do the work

#Pi_output = open("Pi_output.txt", "w")
#Pi_output.write("CHR"  + "\t" + "MID.WIN"  + "\t" + "PI" + "\n")

TajD_output = open(sys.argv[4], "w")
TajD_output.write( "CHR" + "\t" + "Win.Mid" + "\t" + "Win.Len" + "\t" + "Indels" + "\t" + "LowCoverage"+"\t"+ "missing.data" + "\t" + "NUM.SEG.SITES" + "\t" + "WIN.PI" + "\t" + "WIN.THETA" + "\t" + "WIN.TAJD" + "\n" )

windowstart=0
windowend=int(sys.argv[2])

pi_list=[]
snp_positions=[]
low_cov_sites  = 0
indels = 0
missing_data = 0

for line in reader: 
    if len(line.REF)>1 or len(line.ALT)>1: #if an indel, skip this position
        indels = indels +1
        continue
    if line.num_unknown > (n/2) :# if any site has more than half missing data, skip.
        missing_data = missing_data +1
        continue
    
    alleles = []
    for sam in line.samples: #make a list of allele frequencies for samples with enough reads
        if str(sam['GT']) != "None" and sam['DP'] >= dpcutoff:
            gt = sam['GT']
            #print "Genotype call: " +str(gt)            
            alleles.append(int(gt[0]))
            alleles.append(int(gt[-1]))
    if len(alleles) >= n: #if there are enough alleles, calculate pi and add to pilist of the window
        altAF = float(sum(random.sample(alleles, n)))/n
        #if 0.0< altAF < 1.0:
         #   print str(line.POS) +" altAF: " + str(altAF)
        pi = (n/(n-1))*(2.0 * altAF * (1.0-altAF))
        pi_list.append(pi)
        snp_positions.append(line.POS)
    else: #if there arent enough high coverage samples at the site, move on to the next site
        low_cov_sites = low_cov_sites +1
        continue

    if len(pi_list) == int(windowsize):
        midpoint=float((snp_positions[0] + snp_positions[-1])/2)
        windowstart = snp_positions[0]
        windowend = snp_positions[-1]
        print "calculating a window  . . ."
        #print str(pi_list)
        window_pi = myPi(pi_list, midpoint, chrom)
        myTajD(pi_list, window_pi, n, a1, e1 ,e2, windowstart, windowend, midpoint, chrom, low_cov_sites, indels, missing_data)
            #reset for next window
        pi_list=[]
        snp_positions=[]
        low_cov_sites  = 0
        indels = 0
        missing_data = 0

#get the last window
if len(pi_list) >0:
    print "getting last window"
    midpoint=float((snp_positions[0] + snp_positions[-1])/2)
    windowstart = snp_positions[0]
    windowend = snp_positions[-1]
    #print "calculating a window  . . ."
    #print str(pi_list)
    window_pi = myPi(pi_list, midpoint, chrom)
    myTajD(pi_list, window_pi, n, a1, e1 ,e2, windowstart, windowend, midpoint, chrom, low_cov_sites, indels, missing_data)
                                           
TajD_output.close()
#Pi_output.close()

print "pi_list length: " +str(len(pi_list))
print "missing data: " +str(missing_data)
print "low coverage: " +str(low_cov_sites)

print "Complete. Written to " + str(outfile)
