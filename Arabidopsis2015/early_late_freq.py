# -*- coding: utf-8 -*-
"""
Created on Tue Sep  2 14:11:59 2014

@author: Billie
"""

import sys

#sys.path.append("/Users/Billie/Desktop/GENETICS_SOFTWARE/PyVCF-master")
sys.path.append("/data/billie.gould/SCRIPTS/PyVCF-master/")

import vcf

reader = vcf.Reader(open(sys.argv[1], "r"))

early_lines = ['13.136','6.66','2.80','11.64','5.45','15.140','3.23', '12.7', '14.141','6.24','4.10','3.423','8.420','25.191','8.188']
late_lines = ['9.248','7.189','4.295','5.422','19.69','1.74','2.88','27.31','23.404','21.129','1.167','18.89','10.178','16.421','10.139','9.176','20.61']


output=open("EL_allele_freqs.txt", "w")
output.write( "CHR" +"\t"+ "POS" +"\t"+ "TYPE" +"\t" + "LENGTH" +"\t"+ "EARLY.ALT.ALLELES"  +"\t"+ "LATE.ALT.ALLELES"  +"\t"+ "TOT.ALT.ALLELES" +"\t"+ "ALT.allele" +"\n" )
for line in reader:
    early_count=0
    late_count=0
    var_type=line.INFO.get('SVTYPE')
    var_len=line.INFO.get('SVLEN')
    alt_allele=line.ALT
    print str(var_type) + str(alt_allele)
    for sample in early_lines:
        call=line.genotype(sample)
        if call.gt_alleles == ['1','1']:
            early_count=early_count + 2
    for sample in late_lines:
        call=line.genotype(sample)
        #print str(line.CHROM) + "\t"+ str(line.POS) +"\t"+ str(call.gt_alleles)
        if call.gt_alleles == ['1','1']:
            late_count=late_count + 2
    output.write(str(line.CHROM) +"\t"+ str(line.POS) +"\t"+ str(var_type) +"\t"+str(var_len) +"\t"+ str(early_count)+"\t"+ str(late_count) +"\t"+ str(early_count + late_count) +"\t"+ str(alt_allele) +"\n")

output.close()
print "complete. written to EL_allele_freqs.txt"
