# -*- coding: utf-8 -*-
"""
Created on Tue Sep  9 17:04:26 2014

@author: Billie
"""

import sys
print "useage: script earlydata latedata gridpointfile"

###read in EARLY pi and tajima d window data
E_pi_taj_data=open(sys.argv[1], "r")
early_PTvalues=[]
early_PTwindows=[]
C = float((23.0/24.0)/10000.0)
for a in E_pi_taj_data: ##make a list of windows (diff sizes) and their matching Pi and TajD values
    if "CHR" in a:
        continue
    if a.split()[9] == ".":
	continue
    else:
        Ewindowmid=float(a.split()[1])
        Ewindowlen=float(a.split()[2])
        Ewindowstart=Ewindowmid-(Ewindowlen/2)
        Ewindowend=Ewindowmid+(Ewindowlen/2)
        EPi=float(a.split()[7])*C
        ETD=float(a.split()[9])
        early_PTwindows.append([Ewindowstart,Ewindowend])
        early_PTvalues.append([EPi,ETD])
E_pi_taj_data.close()

###read in LATE pi and tajima d window data
L_pi_taj_data=open(sys.argv[2], "r")
late_PTvalues=[]
late_PTwindows=[]
for b in L_pi_taj_data: 
    if "CHR" in b:
        continue
    if b.split()[9] == ".":
	print "window skip"
	continue
    else:    
        windowmid=float(b.split()[1])
        print ("late window midpoint: " + str(windowmid))
	windowlen=float(b.split()[2])
        windowstart=windowmid-(windowlen/2)
        windowend=windowmid+(windowlen/2)
        LPi=float(b.split()[7])*C
        LTD=float(b.split()[9])
        late_PTwindows.append([windowstart,windowend])
        late_PTvalues.append([LPi,LTD])
L_pi_taj_data.close()

##### match gridpoints to windows in the early and late data files
earlyout=open("earlyout.txt", "w")
earlyout.write("gridpoint" +"\t"+"Ewindowstart"+"\t"+"Ewindowend"+"\t"+"Pi"+"\t"+"TajD" +"\n")
lateout=open("lateout.txt", "w")
lateout.write("gridpoint" +"\t" + "Lwindowstart"+"\t"+"Lwindowend"+"\t"+"Pi"+"\t"+"TajD"+"\n")

gridpointfile=open(sys.argv[3], "r")
for point in gridpointfile:
    if "location" in point:
        continue
    else:
        gp=float(point.split()[1])
        #print "gridpoint: " + str(gp)
        Egpdata="NA"
        Lgpdata="NA"
       
        for Eitem in early_PTwindows:   ###look up early data values at gridpoint
            #print "Ewindow" + str(Eitem)
            if Eitem[0] <= gp <= Eitem[1]:
                Egpdata=early_PTvalues[early_PTwindows.index(Eitem)]
                #print "match: " + str(Egpdata)
                earlyout.write(str(gp) +"\t"+ str(Eitem[0]) +"\t" + str(Eitem[1]) + "\t" + str(Egpdata[0]) + "\t" + str(Egpdata[1]) +"\n")
                break  ##move to next gridpoint
            else:
                continue ##move to next window
        if Egpdata == "NA":
            earlyout.write(str(gp)+"\t"+"MISSING VALUE"+"\n")
gridpointfile.close
print "Early data lookup complete."

gridpointfile=open(sys.argv[3], "r")
for point in gridpointfile:
    if "location" in point:
        continue
    else:
        gp=float(point.split()[1])
        #print "gridpoint: " + str(gp)
        Lgpdata="NA"           
        for Litem in late_PTwindows:   ###look up early data values at gridpoint
            #print "Lwindow" + str(Litem)
            if Litem[0] <= gp <= Litem[1]:
                Lgpdata=late_PTvalues[late_PTwindows.index(Litem)]
                #print "match: " + str(Lgpdata)
                lateout.write(str(gp) +"\t"+ str(Litem[0]) +"\t" + str(Litem[1]) + "\t" + str(Lgpdata[0]) + "\t" + str(Lgpdata[1]) +"\n")
                break  ##move to next gridpoint
            else:
                continue ##move to next window
        if Lgpdata == "NA":
            lateout.write(str(gp)+"\t"+"MISSING VALUE"+"\n")
print "Late data lookup complete"            
earlyout.close
lateout.close
gridpointfile.close

print "written to files >> earlyout.txt  >>>lateout.txt "
