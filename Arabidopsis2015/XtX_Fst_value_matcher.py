# -*- coding: utf-8 -*-
"""
Created on Thu Jun 26 18:28:18 2014

@author: Billie
"""

import sys
import re

print 'useage: script Fst.out_file XtX_out_file. all XtX entries must be present in the Fst file'
#for each line in the fst.out table, split the line, define locusid, match locusid with Fst value, add as dict element:

Fstdata=open(sys.argv[1], "r")
print "creating dictionary . . ."
dictFst={}
for line in Fstdata:
    locusid=str(line.split()[0] + ":" + line.split()[1])
    Fst=line.split()[2]
    dictFst[locusid]=Fst
Fstdata.close()
print 'DONE. matching values . . .'

#for each line in the XtX output file, translate coord to locusid, lookup Fst in dictionary, write locusid, XtX and Fst to output table

XtXdata=open(sys.argv[2], "r")
output=open("XtX_Fst_table.txt", "w")
for line in XtXdata:
    filename=line.split()[0]
    chrm=str(re.split('\_|\.', filename)[2])
    pos=str(re.split('\_|\.', filename)[3])
    locusid=str(chrm + ":" + pos)
    XtX=line.split()[1]
    Fstlookup=dictFst[locusid]
    output.write(chrm + "\t" +  pos + "\t" + str(XtX) + "\t"+ str(Fstlookup) + "\n")

XtXdata.close()
print 'COMPLETE: XtX_Fst_table.txt'
