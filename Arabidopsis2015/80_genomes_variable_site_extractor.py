# -*- coding: utf-8 -*-
"""
Created on Thu Nov 13 12:30:24 2014

@author: Billie
"""
##this script takes the Cao et al. 80 genomes snp data file and extracts variable sites. Useage: script 80genomes.txt
import sys

Csnps=open(sys.argv[1], "r")
variable_sites=open("variable_sites.txt", "w")
variable_sites.write("Chr" +"\t"+ "Pos" +"\n")
var_sites=0
sites=0
missing_data_chars=("Z","N","R")  ##fyi, "D" and "-" indicate deletions.see readme file
print "Processing . . ."

for line in Csnps:
    sites=sites+1
    chr=line.split()[0]
    pos=line.split()[1]
    data=line.split()[3:]
    missing_data=sum(data.count(x) for x in missing_data_chars) ## if line has more than 50% missing data, skip
    if missing_data > 40:
        continue
    else:
        data2=[item for item in data if item not in missing_data_chars]   ##god, python is beautiful . . . . 
        if data2[4:]!=data2[3:-1]:  #if all the elemens in the genotypes are not the same, i.e. site is variable, write
            variable_sites.write(str(chr) +"\t"+ str(pos) +"\n")
            var_sites=var_sites+1
        else:
            continue

variable_sites.close()
Csnps.close()

print "Complete."
print "Processed " + str(sites) + " sites in the file."
print "There were " +str(var_sites)+ " variable sites."
print "Written to >>> variable_sites.txt <<<"

