# -*- coding: utf-8 -*-
"""
Created on Thu May 15 17:33:32 2014

@author: Billie
"""
### this script will take a single SORTED VCF (Chr/Pos) and write only the unique entries into a new VCF. for repeat entries (at the same position), it calculates the total number of supporting reads and records the variant with the higher number only. 
## currently, commented out lines function to regenotype data as missing at a certain threshold.

import sys
#import re

print "make sure the input file is sorted by chr and position" 
print "processing . . ."

output=open("SVs_unique.vcf.txt", "w")
repeat_SVs=open("repeat_SVs.txt", "w")

data=open(sys.argv[1], "r")
uniq_positions=["start"]
count_list=[]
prev_line=""

for line in data:
    if "#" in line:
        output.write(line)
    else:                           #take the sample data from the line, A) sum all the individual supporting reads, B)regenotype the line
       line_snp=line.split()[0:2]
       rc= line.split("\t")
       X=rc[9:42]
#        new_line=rc[0:9]
       count=0
       for indv in X: 
           indv_reads=indv.split(",")[1]
           count=count+int(indv_reads)     #(A)
       print "read count: " + str(count)
       if line_snp != uniq_positions[-1]: #if the snp is at a different pos than the last one 
            uniq_positions.append(line_snp)
            count_list.append(count)
            output.write(line)
#            output.write("\t".join(new_line) + "\n") #writes the previous line, the ONLY time a line is written to output vcf is if the next snp read in is at a different position
#            prev_line=new_line #saves the current regenotyped line 
       elif count >= count_list[-1]: #if the snp is at the same position as the last one, compare the number of supporting reads to the last one, if greater
            count_list.append(count)
            output.write(line)
            #prev_line=new_line  #saves the current line
       else: #if the counts of the line are less than or fewer than the previous matching variant
            repeat_SVs.write(line)
            #prev_line=new_line
            continue # throw out the line and move to next line
            
#output.write("\t".join(prev_line)) #record the last line in the file

data.close()
output.close()
repeat_SVs.close()

print "there are " + str(len(uniq_positions)-1) + " unique positions from pindel"
print "remember to convert 0/0 to missing data"
print "written to >>>>>SVs_unique.vcf<<<< and >>>>Repeat_SVs.txt"

