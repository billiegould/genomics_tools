# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import sys
#sys.path.append("/Users/Billie/Desktop/PyVCF-master")
sys.path.append("/data/billie.gould/SCRIPTS/PyVCF-master/")

#print "Input VCF containing only snps and samples of interest (generated using vcftools --positions)

# arguments input.vcf windowsize chrm#

import vcf
#import random
#import math

inputVCF=sys.argv[1]


#Chrm_lengths={1:30427671, 2:19698289, 3:23459830, 4:18585056, 5:26975502}
#chrmlength=Chrm_lengths[chrom]

reader = vcf.Reader(open(inputVCF, "r"))
sample_names=reader.samples

output = open("genotypes.txt", "w")
output.write( "CHR" + "\t" + "Pos" + "\t" + "REF"+ "\t" + "ALT" + str("\t".join(sample_names[:])) +"\n")
indels=0

for record in reader:
    print record
    alt=str(record.ALT[0])
    ref=str(record.REF[0])
    if len(alt)>1 or len(ref)>1:
        indels = indels + 1
        continue
    else:
        output.write(str(record.CHROM) +"\t"+ str(record.POS) +"\t"+ str(record.REF[0]) +"\t"+ str(record.ALT[0]) +"\t")
    for sample in record.samples:
        if sample['GT'] == '0/0':
            gt=(ref + ":" + ref)
        elif sample['GT'] == "0/1":
            gt=(ref + ":" + alt)
        elif sample['GT'] == "1/1":
            gt=(alt + ":" + alt)
        else:
            gt='N:N'
	output.write( str(gt) +"\t")
    output.write("\n")
    
print " there were " + str(indels) + " indels skipped"
print "written to >genotypes.txt<"

output.close()

