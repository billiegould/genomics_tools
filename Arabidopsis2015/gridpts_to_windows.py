# -*- coding: utf-8 -*-
"""
Created on Wed Jul  2 11:25:39 2014

@author: Billie
"""
###replace "chr" in gff with number only!!!!!
# this script will compare snps in a VCF (or other Chr/Pos list) to regions of interest in another file ..formatted chr win_Start win_End 
#useage: script VCF.vcf regions.txt


import sys

#create a dictionary with a list of regulatory regions per chromosomes
chr1listSTR=[]
chr2listSTR=[]
chr3listSTR=[]
chr4listSTR=[]
chr5listSTR=[]
windowfile=open(sys.argv[2], "r")
for line in windowfile:
    if "CHROM" in line.split():
        continue
    else:
    	win_mid=float(line.split()[1])
    	win_len=float(line.split()[2])
    	win_start=int(win_mid-(win_len/2.0))
    	win_end=int(win_mid+(win_len/2.0))
    	if int(line.split()[0]) == 1:
            chr1listSTR.append([win_start,win_end])
    	elif int(line.split()[0]) == 2:
            chr2listSTR.append([win_start,win_end])
    	elif int(line.split()[0]) == 3:
            chr3listSTR.append([win_start,win_end])
    	elif int(line.split()[0]) == 4:
            chr4listSTR.append([win_start,win_end])
    	elif int(line.split()[0]) == 5:
            chr5listSTR.append([win_start,win_end])

chr1listNUM=[map(float, x) for x in chr1listSTR]
chr2listNUM=[map(float, x) for x in chr2listSTR]
chr3listNUM=[map(float, x) for x in chr3listSTR]
chr4listNUM=[map(float, x) for x in chr4listSTR]
chr5listNUM=[map(float, x) for x in chr5listSTR]

windict = {}
windict[1]=chr1listNUM
windict[2]=chr2listNUM
windict[3]=chr3listNUM
windict[4]=chr4listNUM
windict[5]=chr5listNUM
print "window dictionary constructed, starting comparisons . . ."
windowfile.close()
#print str(windict[5][0:6])

#for snp in VCF, retieve correct chr RegRegion list, see if it falls in any of them 
output=open("output.txt", "w")
output.write("SNP.CHR" + "\t" + "SNP.POS" + "\t" + "REG.START" + "REG.END" + "\n")
counter = 0
VCF=open(sys.argv[1], "r")  #or for gridpoint in list
for line in VCF:
    counter += 1
    snp=line.split()
    if counter == 1:
#        print "Processing variant: " + str(snp[0:2])
        counter = 0
    if "#" in snp[0]:
        continue
    else:
        snpchr=int(snp[0])
        snppos=int(snp[1])
        windowlist=windict[snpchr]
	#print "got list of windows"
#COMPARE EACH SNP TO ALL WINDOWS IN CHR
        for window in windowlist:
            windowstart=window[0]
            windowend=window[1]
            if snppos < windowstart:
                break
#                print "snp " + str(snppos) + " non-overlapping with window " + str(windowstart)            
            elif snppos >= windowstart and snppos <= windowend:
                print "MATCH at " + str(snppos)
                output.write(str(snpchr) + "\t" + str(snppos) + "\t" + str(windowstart) + "\t" + str(windowend) + "\n")
            else:
#		print "variant pos: " +str(snppos)+ "window: " +str(windowstart) + "::" +str(windowend)
                continue
output.close()
VCF.close()
print "Done. printed to 'output.txt' "
