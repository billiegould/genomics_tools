# -*- coding: utf-8 -*-
"""
Created on Wed Jul  2 11:25:39 2014

@author: Billie
"""
###
# this script will find SNPs inside a list of windows and eliminate them, and write out new VCF.. . SORT and replace chr1 etc with '1' "
#useage: script VCF_file window_file.txt . .. files must be sorted, there must be at least one window per chromosome"

import sys

#create a dictionary with a list of target windows per chromosomes
chr1listSTR=[]
chr2listSTR=[]
chr3listSTR=[]
chr4listSTR=[]
chr5listSTR=[]
windowfile=open(sys.argv[2], "r")
for line in windowfile:
    if '#' in line.split():
        continue
    elif int(line.split()[0]) == 1:
        chr1listSTR.append((line.split()[1:3]))
    elif int(line.split()[0]) == 2:
        chr2listSTR.append((line.split()[1:3]))
    elif int(line.split()[0]) == 3:
        chr3listSTR.append((line.split()[1:3]))
    elif int(line.split()[0]) == 4:
        chr4listSTR.append((line.split()[1:3]))
    elif int(line.split()[0]) == 5:
        chr5listSTR.append((line.split()[1:3]))

chr1listNUM=[map(float, x) for x in chr1listSTR]
chr2listNUM=[map(float, x) for x in chr2listSTR]
chr3listNUM=[map(float, x) for x in chr3listSTR]
chr4listNUM=[map(float, x) for x in chr4listSTR]
chr5listNUM=[map(float, x) for x in chr5listSTR]

windict = {}
windict[1]=chr1listNUM
windict[2]=chr2listNUM
windict[3]=chr3listNUM
windict[4]=chr4listNUM
windict[5]=chr5listNUM
print "window dictionary constructed, starting snp modifications . . ."
windowfile.close()

#collect list of snps in the file that fall within the windows
output=open("output.vcf.txt", "w")
VCF=open(sys.argv[1], "r")
filelines=0
for line in VCF:
    if '#' in line:
        output.write(line)
    else:
        snpdata=line.split('\t')
        snpchr=int(snpdata[0])
        snppos=int(snpdata[1])
        windowlist=windict[snpchr]
#for snp, see if it overlaps with each window, 
        for window in windowlist:
            windowstart=window[0]
            windowend=window[1]
            if snppos < windowstart:
                output.write(line)
                filelines=filelines+1
                #snp is after the previous window (but before the current window), write out, do next snp
                break
            elif snppos <= windowend:
                output.write(str(snpdata[0]) +"\t"+ str(snpdata[1]) +"\t"+ str(snpdata[2]) +"\t"+ str(snpdata[3]) +"\t"+ str(snpdata[4]) +"\t"+ str(snpdata[5]) +"\t"+ str(snpdata[6]) +"\t"+ str(snpdata[7]) +"\t"+ str(snpdata[8]) + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." + "\t" + "./.:.:.:.:." +"\n")
		filelines=filelines+1
                break
                #snp is in the window, make sample 8.188 missing data, next snp
            else:
                continue
               #snp is after the end of the window, move to next window
        if snppos > windowlist[-1][-1]:
             output.write(line)
             filelines=filelines+1
             #write out all the snps that fall after the last window on the chromosome             
output.close()
VCF.close()
print "Completed. The output.vcf has " +str(filelines)+ " variants."
