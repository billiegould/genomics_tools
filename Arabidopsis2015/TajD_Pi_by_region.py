# -*- coding: utf-8 -*-

import sys
#sys.path.append("/Users/Billie/Desktop/PyVCF-master")
sys.path.append("/data/billie.gould/SCRIPTS/PyVCF-master/")

print "this script excludes indels, and sites with more than 50% missing genotype calls, an genotype calls with coverage below a cutoff"



import vcf
import pysam
import random
import math

inputVCF=sys.argv[1]
#windowsize=float(sys.argv[2])
#chrom=int(sys.argv[3])
genelist=sys.argv[2]
outfile=sys.argv[3]
dpcutoff=int(sys.argv[4])
n=float(sys.argv[5])*2

#Chrm_lengths={1:30427671, 2:19698289, 3:23459830, 4:18585056, 5:26975502}
#chrmlength=Chrm_lengths[chrom]

#reader = vcf.Reader(open(inputVCF, "r"))
#sample_names=reader.samples
#_N=len(sample_names)


#Initialize TajD variables:  
#chromsampSize = _N*2  
a1 = 0
a2 = 0
for i in range(1, int(n)): #sum is for up to N-1,since python does not include the last item in the range, we're covered here.
    a1 += 1/float(i)
    a2 += 1/(float(i)*float(i)) # see connor and hartl p. 175

b1 = float(n+1)/(3*(n-1))#see Tajima 1989
b2 = float(2*(n*n +n+3))/(9*n*(n-1))

c1 = (b1) - 1/a1
c2 = b2-((float(n+2))/(a1*n))+(a2/(a1*a1))

e1 = c1/a1
e2 = c2/(a1*a1+a2)


def myPi(pi_list, chrom, genestart, geneend):
    print "Number of Site pi values: " + str(len(pi_list))	
    region_pi=float(sum(pi_list))/float(len(pi_list))  # average all the individual site pi values
    print "Region Pi equals " + str(region_pi)
    #Pi_output.write( str(chrom) + "\t" + str(midpoint) + "\t" + "\t" + repr(window_avgpi) + "\n")
    return region_pi

def myTajD(pi_list, region_pi):
    S=0
    for p in pi_list: #total number of polymorphic sites in the window, S
        if 0 < p < 1:
            S=S+1
    if S != 0: #calculate Tajima's D
        theta = float(S)/a1
        #print("theta ", theta)
        d = float(region_pi *len(pi_list)) - theta ###ths is Tajima's D, raw. pi for the sequence is adjusted to a sum rather than a per site estimate
        p1 = e1*S
        p2 = e2*S*(S-1)
        denom=float(math.sqrt(p1+p2)) #denominator is the expected variance in d
        #print "sqrtvariance= " +str(denom)
        D = d/denom
        print "TajD= " +str(D)
    else:
        theta = "no seg. sites"
        D = 'NA'
    return (S, theta, D)
    
#############do the work

output = open(sys.argv[3], "a")
output.write("Gene" +"\t"+ "CHR" + "\t" + "region_start" + "\t" + "region_end" + "\t" + "percent_sites_used" +"\t"+ "indels" + "\t" + "low_cov_sites"+"\t"+ "missing_data" + "\t" + "seq_sites(S)" + "\t" + "PI" + "\t" + "THETA" + "\t" + "TAJD" + "\n" )

reader = vcf.Reader(open(inputVCF, "r"))
sample_names=reader.samples
print "Samples: " +str(sample_names)

gene_list=open(genelist, "r")

for line in gene_list:
    genename=line.split()[0]
    chrom=int(line.split()[1])
    genestart=int(line.split()[2])
    geneend=int(line.split()[3])
    print str(genename)
    print str(genestart) + " to " +str(geneend)
    region = reader.fetch(chrom, (genestart-1), geneend)
    pi_list=[]
    snp_positions=[]
    low_cov_sites  = 0
    indels = 0
    missing_data = 0
    for site in region:
        alleles = []
        if len(site.REF)>1 or len(site.ALT)>1: #if an indel, skip this position
            indels = indels +1
            continue
        if site.num_unknown > (n/2):# if any site has more than half missing data, skip.
            missing_data = missing_data +1
            continue
        for samp in site.samples: #make a list of allele frequencies for samples with enough reads
            if str(samp['GT']) != "None" and samp['DP'] >= dpcutoff:
                gt = samp['GT']
                #print "Genotype call: " +str(gt)            
                alleles.append(int(gt[0])) 
                alleles.append(int(gt[-1]))
        if len(alleles) >= n: #if the list of alleles is equal to or greter than the the numberof chromosomes sampled, subsample an calculate pi atthat site and add to pilist
            #print str(alleles) +"\t n= "+ str(n)
            altAF = float(sum(random.sample(alleles, int(n))))/int(n)
            pi = float(n/(n-1))*(2.0 * altAF * (1.0-altAF))  #pi is equal to 2pq * (n/n-1) see Tajima 1989
            pi_list.append(pi)
            snp_positions.append(site.POS)
        else: #if there arent enough high coverage samples at the site, move on to the next site
            low_cov_sites = low_cov_sites +1
            continue
    #At the end of the region, calculatetotal pi an TajD for the gene region:    
    #print str(pi_list)
    percent_used= len(pi_list)/(float(geneend)-genestart) *100
    if len(pi_list)<((geneend-genestart)/2):
         print "not enough quality sites"
         output.write(str(genename) + "\t" + str(chrom) + "\t" + str(genestart) + "\t" + str(geneend)+ "\t"+ str(percent_used)+ "\t" +str(indels) + "\t" +str(low_cov_sites) + "\t" +str(missing_data) + "\t" + "NA" + "\t" + "NA" + "\t"+ "NA" + "\t"+ "NA" + "\n")
    else:    
         region_pi = myPi(pi_list, chrom, genestart, geneend)   
         TD_out = myTajD(pi_list, region_pi)
    #print "S, theta, D : " + str(TD_out[0])+ "\t" + str(TD_out[1])+ "\t" + str(TD_out[2])
    ##("CHR" + "\t" + "region_start" + "\t" + "region_end" + "\t" + "indels" + "\t" + "low_cov_sites"+"\t"+ "missing_data" + "\t" + "seq_sites(S)" + "\t" + "PI" + "\t" + "THETA" + "\t" + "TAJD" + "\n" )        
         output.write(str(genename) + "\t" + str(chrom) + "\t" + str(genestart) + "\t" + str(geneend)+ "\t"+ str(percent_used)+ "\t" +str(indels) + "\t" +str(low_cov_sites) + "\t" +str(missing_data) + "\t" +str(TD_out[0]) + "\t" +str(region_pi) + "\t"+str(TD_out[1]) + "\t"+ str(TD_out[2]) + "\n")

output.close()

print "Complete. Written to " + str(outfile)

