## this script will take a gff file and store all entries into a dictionary by scaffold. dictionary is then stored as a pickle object for quick reference.

import sys
import pickle


interval_dict={}

#open and read file into dictionary object
file = open(sys.argv[1], "r")
for line in file:
    gene = line.split()[8][4:15]
    scaffold = line.split()[0]
    reg_start = line.split()[3]
    reg_end = line.split()[4]
    interval = (gene, reg_start,reg_end)

    if scaffold in interval_dict:
        interval_dict[scaffold].append(interval)
    else:
        interval_dict[scaffold]=[interval]

file.close()

# store dictionary as external python object
intervals = open((sys.argv[1] + ".pkl"), "wb")        #writable as binary
pickle.dump(interval_dict, intervals)                   # external object saved in directory where the input file is
intervals.close()

