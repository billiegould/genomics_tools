# -*- coding: utf-8 -*-
"""
Created on Mon Jul  7 16:18:11 2014

@author: Billie
"""

#ths script will calculate ratio of heterozygous snps to homozygous snps in  windows from an input VCF: useage: script VCF windowsize

import sys
import re
import math

#define how to calculate heterozygosity for each chromosome:
def hetz(L):
    CHR=str(L[0][0])
    chrlength=int(L[-1][1])
    windowstart=0
    windowend=int(sys.argv[2])
    #analysis for EACH WINDOW read in all relevant snpdata
    for w in range(0, int(math.ceil(chrlength/int(sys.argv[2])))):    
        hets=0
        homs=0        
        for item in L:
            pos=int(item[1])
            if pos < windowstart:
                continue
            elif pos > windowend:
                output.write(str(CHR) + "\t" + str(windowstart) + "\t" + str(windowend) + "\t" + str(hets) + "\t" + str(homs) + "\n")              
                windowstart=windowend + 1
                windowend=windowstart + (int(sys.argv[2])-1)                 
                break
                #moves to define next window
            else:
                genotype=item[-1]
                genotype2=genotype.split(':')
                hets=hets + genotype2.count("0/1")
                homs=homs + genotype2.count("1/1") + genotype2.count("0/0")
                #moves to read in next snp in L list



#for each individual in sample data columns 8-41 do the work:
for samplecol in range(9,42):
#record sample name
    VCF=open(sys.argv[1], "r")
    for line in VCF:
        if "##" in line.split()[0]:
            continue
        elif "#CHROM" in line.split()[0]:
            samplename=line.split()[samplecol]
            print "Processing sample " + str(samplename) + " . . ."
            break
    VCF.close()
#generate one empty list per chromosome    
    chr1listSTR=[]
    chr2listSTR=[]
    chr3listSTR=[]
    chr4listSTR=[]
    chr5listSTR=[]
#read the snp data for each chromosome into the chromosome lists
    VCF=open(sys.argv[1], "r")    
    for line in VCF:
        if "#" in line.split()[0]:
            continue
        elif int(line.split()[0]) == 1:
            data=re.split('\t', line)
            data2=data[0:9]
            data3=data2 + [data[samplecol]]
            chr1listSTR.append(data3)
        elif int(line.split()[0]) == 2:
            data=re.split('\t', line)
            data2=data[0:9]
            data3=data2 + [data[samplecol]]
            chr2listSTR.append(data3)
        elif int(line.split()[0]) == 3:
            data=re.split('\t', line)
            data2=data[0:9]
            data3=data2 + [data[samplecol]]
            chr3listSTR.append(data3)
        elif int(line.split()[0]) == 4:
            data=re.split('\t', line)
            data2=data[0:9]
            data3=data2 + [data[samplecol]]
            chr4listSTR.append(data3)
        elif int(line.split()[0]) == 5:
            data=re.split('\t', line)
            data2=data[0:9]
            data3=data2 + [data[samplecol]]
            chr5listSTR.append(data3)

    print "Data read for " + str(samplename) 
    VCF.close()

#create an output file for the sample and open it
    outfile= str(samplename)+"_hetwindows.txt"
    output=open(outfile, "w")
    output.write("CHR" + "\t" + "WIN.START"+ "\t" +"WIN.END"+ "\t" + "NUM.HET.GTs" + "\t" + "NUM.HOM.GTs" + "\n")
        
##run hetz calculateion on each chromosome: 
    hetz(chr1listSTR)
    print "Completed Chr1, individual " + str(samplename)
    hetz(chr2listSTR)
    print "Completed Chr2, individual " + str(samplename)
    hetz(chr3listSTR)
    print "Completed Chr3, individual " + str(samplename)
    hetz(chr4listSTR)
    print "Completed Chr4, individual " + str(samplename)
    hetz(chr5listSTR)
    print "Completed Chr5, individual " + str(samplename)
#close the output file for the individual:
    output.close()

print "COMPLETE!"
