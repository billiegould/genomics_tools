# -*- coding: utf-8 -*-
"""
Created on Thu May 15 17:33:32 2014

@author: Billie
"""
### this script will take a single SORTED file of entries and extract unique entries by the specified column below.
import sys

#if item in list does not match item just added to uniques then add it to uniques
print "make sure the input file is sorted by the column to extract by" 

data=open(sys.argv[1], "r")
uniqlist=["start"]
outlist=[]
for line in data:
    if line.split()[0] != uniqlist[-1]:
	print line.split()[0]
        gene = line.split()[0]
	uniqlist.append(gene)
	outlist.append(line)
data.close()

print "there are " + str(len(outlist)-1) + "unique elements in the list: " #subtract one from outlist because of file header
print  "writing output to uniques.txt. . ."

snps=open("uniques.txt", "w")
for item in outlist:
    snps.write(item)
snps.close()

print "complete. writen to file: uniques.txt"
