## this script will convert text file of scaffold lengths into a stored python object.

import sys
import pickle

dict={}

#open and read file into dictionary object
file = open(sys.argv[1], "r")
for line in file:
    scaffold = line.split()[0]
    length = line.split[1]
    dict[scaffold]=length
file.close()

# store dictionary as external python object
lengths = open((sys.argv[1] + ".pkl"), "wb")        #writable as binary
pickle.dump(dict, lengths)                   # external object saved in directory where the input file is
lengths.close()

