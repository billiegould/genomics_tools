# -*- coding: utf-8 -*-
"""
Created on Fri May 23 17:12:39 2014

@author: Billie
"""

import sys

filelist=[]
myfile=open(sys.argv[1], "r")
for line in myfile:
    filelist.append(line)
myfile.close()

output=open("subsampled.txt", "w")
index = 0
for i in range(0, len(filelist)/16):
    output.write(filelist[index])
    output.write(filelist[index+1])    
    index = index + 16  #samples 2 lines every 16 lines

output.close()