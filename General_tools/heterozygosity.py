# -*- coding: utf-8 -*-
"""
Created on Mon Jul  7 16:18:11 2014

@author: Billie
"""

#ths script will calculate ratio of heterozygous snps to homozygous snps across ALL individuals in  windows from an input VCF.
# useage: script VCF windowsize

import sys


vcf = open(sys.argv[1], "r")
output = open("hetwindows.txt", "w")
output.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % ("chrom", "win_start", "win_end", "win_hets", "win_calls", "ref_Ns", "perc_hets"))

chrom_prev = "start"

for line in vcf:
    if "#" in line:     #skip header lines
        continue

    chrom_new = line.split()[0]

    #check if starting a new chromosome. if so, reset lists and counts.
    if chrom_new != chrom_prev:
        win_hets = 0
        win_calls = 0
        win_Ns = 0
        positions = []
        chrom_prev = chrom_new
        print("processing chromosome: %s" % (chrom_new))

    positions.append(line.split()[1])

    if line.split()[3] == "N":
        win_Ns += 1

    # sum the hets and called sites for each line. add values to window total
    line_hets = 0
    line_calls = 0
    for i in range(9,97,1):
        #print(i)
	    if line.split()[i].split(":")[0] == "./.":
            continue
        elif line.split()[i].split(":")[0] == "0/1":
            line_hets +=1
            line_calls +=1
        else:
            line_calls +=1
    win_hets += line_hets
    win_calls += line_calls

    #check if window contains enough sites, write window value to output. reset counts and lists
    if  len(positions) == 50:
        perc_hets = win_hets/float(win_calls)
        win_start = positions[0]
        win_end = positions[-1]
        output.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (chrom_new, win_start, win_end, win_hets, win_calls,win_Ns perc_hets))
        win_hets = 0
        win_calls = 0
        positions = []
        win_Ns = 0
    else:
        continue

vcf.close()
output.close()
print("Complete. Nice Job.")