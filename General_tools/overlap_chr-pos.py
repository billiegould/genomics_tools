# -*- coding: utf-8 -*-
"""
Created on Mon Apr 14 11:54:51 2014

@author: Billie
"""
###usage : python overlap.py datafile1.txt datafile2.txt outfile
# chromosome and position of marker must be in col 1 and col 2 in both files
## this works on lists of tuples, i.e. chr,pos pairs. Vcf coords do not have to be split by chromosome this way. 
#output to matches.txt

import sys

output=open(sys.argv[3], "w")

list1=open(sys.argv[1], "r")
list1pos=[]
for line in list1:
    list1pos.append(line.split()[0:2])
list1.close()

list2=open(sys.argv[2], "r")
list2pos=[]
for line in list2:
    list2pos.append(line.split()[0:2])
list2.close()

matches=[rec for rec in list2pos if rec[0:2] in list1pos]  #return records in list2 if the first two entries in the record match the item in list 1
print "There are " + str(len(matches)) + " matching positions."

for item in matches:    
    output.write(str(item[0]) + "\t"+ str(item[1])+ "\n")
output.close()

print "complete. written to " + sys.argv[3]

