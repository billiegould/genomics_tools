# -*- coding: utf-8 -*-
"""
Created on Mon Jul  7 16:18:11 2014

@author: Billie
"""

#ths script will calculate window ratios of heterozygous snps to homozygous snps for A SELECTED SAMPLE IN an input VCF.

# useage: script VCF windowsize sample_number_to_process

import sys

samplecol=int(sys.argv[3]) + 8

vcf = open(sys.argv[1], "r")
for line in vcf:
    if "##" in line:     #skip header lines
        continue
    elif "#CHROM" in line.split()[0]:
        samplename=line.split()[samplecol]
        print "Processing sample " + str(samplename) + " . . ."
        break
vcf.close()


output = open((str(samplename) +"_hetwindows.txt"), "w")
output.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % ("chrom", "win_start", "win_end", "win_hets", "win_calls", "ref_Ns", "perc_hets"))

chrom_prev = "start"

vcf = open(sys.argv[1], "r")
for line in vcf:
    if "#" in line:     #skip header lines
        continue

    chrom_new = line.split()[0]

    #check if starting a new chromosome. if so, reset lists and counts.
    if chrom_new != chrom_prev:
        win_hets = 0
        win_calls = 0
        win_Ns = 0
        positions = []
        chrom_prev = chrom_new
        print("processing chromosome: %s" % (chrom_new))

    positions.append(line.split()[1])

    if line.split()[3] == "N":
        win_Ns += 1

    if line.split()[samplecol].split(":")[0] == "./.":
        continue
    elif line.split()[samplecol].split(":")[0] == "0/1":
        win_hets += 1
        win_calls += 1
    else:
        win_calls += 1

    #check if window contains enough sites, write window value to output. reset counts and lists
    if  win_calls == int(sys.argv[2]):
        perc_hets = win_hets/float(win_calls)
        win_start = positions[0]
        win_end = positions[-1]
        output.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (chrom_new, win_start, win_end, win_hets, win_calls, win_Ns, perc_hets))
        win_hets = 0
        win_calls = 0
        positions = []
        win_Ns = 0
    else:
        continue

vcf.close()
output.close()
print("Complete hetwindows for sample " + str(samplename))