## This script will take the output of two blast searches and return the reciprocal best (first) hits in list form with alignment ranges from the best alignment.

## This script modified from https://github.com/hongqin/Simple-reciprocal-best-blast-hit-pairs/blob/master/RBH-v1.py by Hong Qin

# input file format:
# 0:qseqid 1:sseqid 2:pident 3:length 4:mismatch 5:gapopen 6:qstart 7:qend 8:sstart 9:send 10:evalue 11:bitscore 12:qcovhsp
Usage = """RBH BLASTOUTPUT1 BLASTOUTPUT2 > RBH-list-outfile &2>run.log """

import sys, re

if len(sys.argv) < 3:
	print(Usage)

def blast_list_parser(open_file):
	D = {} #dictionary for BLAST file
	# prev_gene_data = [queryID, queryId_alnA_start, queryId_alnA_end, subjectId, subjectId_alnA_start, subjectId_alnA_end, alnA_eval, pct_cov, num_mismatches, num_gaps]
	hsp_list = [ ("#", 0, 0, "", 0, 0, 1, 0) ]
	num_mult_hsps = 0
	for Line in open_file:
		if ( "#" not in Line):
			data=Line.strip()
			Elements = re.split('\t', data)
			line_data = (Elements[0], Elements[6], Elements[7], Elements[1], Elements[8], Elements[9], Elements[10], Elements[12], Elements[4],Elements[5])
			queryId = Elements[0]	
		#	queryId_alnA_start = Elements[6]
		#    queryId_alnA_end = Elements[7]
		#	subjectId = Elements[1]
		#    subjectId_alnA_start = Elements[8]
		#    subjectId_alnA_end = Elements[9]
			#sys.stderr.write( "QID: " + str(queryId) + " LastHSP: " +str(hsp_list[-1][0]) )
			if queryId == hsp_list[-1][0]:			# if the present gene matches the previous gene (is a multiple HSP), add dataline to list
				hsp_list.append(line_data)

			if queryId != hsp_list[-1][0]:	# if the present gene doesnt match the last hsp
				
				if len(hsp_list) > 1: num_mult_hsps += 1
				sum_hsp_cov = float(0.0) 			# sum the coverage for the prev hsps
				perc_cov_list = []
				
				for dataline in hsp_list:
					perc_cov = int(dataline[7])
					perc_cov_list.append(perc_cov)
					sum_hsp_cov = sum_hsp_cov + perc_cov
				if sum_hsp_cov >= 75:			#if the sum is greater than 75%, add longest coverage hsp to dict
					index = perc_cov_list.index(max(perc_cov_list))
					D[hsp_list[index][0]] = hsp_list[index][1:]	
				hsp_list = [(line_data)]		# start a new hsp list with the current data line
		
	if len(hsp_list) > 0:					#write the last HSP
		sum_hsp_cov = int(0)                         # sum the coverage for the prev hsps
                perc_cov_list = []
                for dataline in hsp_list:
                       perc_cov = int(dataline[7])
                       perc_cov_list.append(perc_cov)
                       sum_hsp_cov = sum_hsp_cov + perc_cov
                       if sum_hsp_cov >= 75:                   #if the sum is greater than 75%, add longest coverage hsp to dict
                                index = perc_cov_list.index(max(perc_cov_list))
                                D[hsp_list[index][0]] = hsp_list[index][1:]

	sys.stderr.write("Parsed a file.\n")
	return (D, num_mult_hsps)

#########################################
sys.stderr.write(" \n \n Input files must be sorted by queryID.\n\n")

infl1 = sys.argv[1]
infl2 = sys.argv[2]

#parse first BLAST results
fileA = open(infl1, 'r')
(D1,N) = blast_list_parser(fileA)
sys.stderr.write("Num_mult_hsps: " + str(N) +"\n")
fileA.close()

#sys.exit()

#parse second BLAST results with "B" alignments
fileB = open(infl2, 'r')
(D2,N) = blast_list_parser(fileB)
sys.stderr.write("Num_mult_hsps: " + str(N) +"\n")
fileB.close()

#Now, pick the share pairs
# D[gene] = [ queryId_alnA_start, queryId_alnA_end, subjectId, subjectId_alnA_start, subjectId_alnA_end, alnA_eval, pct_cov]

SharedPairs={}
for gene_name in D1.keys():
	#sys.stderr.write(str(gene_name))
	match_name = D1[gene_name][2]     #gene1 is matched by gene2
	if ( match_name in D2.keys() ):     # if gene2 is in dictionary 2
		if ( D2[match_name][2] == gene_name) : # . . . and its match is gene1 (This is a reciprocal best hit. Doesnt matter which fileyou start from)
		   # sys.stderr.write("RBB hit! \n")
		    if float(D1[gene_name][5]) < float(D2[match_name][5]):  #if alignment A has a lower or equal e-val, return output from dict.A
				L1_gene = "L1-"+ str(gene_name)
				S1_gene = "S1-"+ str(D1[gene_name][2])
				SharedPairs[gene_name] = (L1_gene, '\t'.join(D1[gene_name][0:2]), S1_gene, '\t'.join(D1[gene_name][3:]))
				#print(str(SharedPairs[gene_name]) +"\n")
		    elif float(D1[gene_name][5]) == float(D2[match_name][5]):
					
				if int(D1[gene_name][6]) >= int(D2[match_name][6]):
					L1_gene = "L1-"+ str(gene_name)
                                	S1_gene = "S1-"+ str(D1[gene_name][2])
                                	SharedPairs[gene_name] = (L1_gene, '\t'.join(D1[gene_name][0:2]), S1_gene, '\t'.join(D1[gene_name][3:]))
				elif int(D1[gene_name][6]) < int(D2[match_name][6]):
					S1_gene = "S1-" + str(match_name)
                                	L1_gene = "L1-" + str(D2[match_name][2])
                                	SharedPairs[match_name] = (S1_gene, '\t'.join(D2[match_name][0:2]), L1_gene, '\t'.join(D2[match_name][3:]))

            	    elif float(D1[gene_name][5]) > float(D2[match_name][5]): #if alignment B has a lower e-val, return output from dict.B
				S1_gene = "S1-" + str(match_name)
				L1_gene = "L1-" + str(D2[match_name][2])
                    		SharedPairs[match_name] = (S1_gene, '\t'.join(D2[match_name][0:2]), L1_gene, '\t'.join(D2[match_name][3:]))
	#	else: 
	#	    sys.stderr.write("No match\n")
	#else:
	 #   sys.stderr.write("No match\n")


sys.stdout.write("gene1\tg1_start\tg1_end\tgene2\tg2_start\tg2_end\te-val\tpct_cov\tnum_mismatch\tnum_gaps\n")

count = 0
for entry in SharedPairs.keys(): 
	line = str("\t".join(SharedPairs[entry]))
	if int(line.split('\t')[8]) != 0 or int(line.split('\t')[9]) != 0:			#if there is at least one mismatch or gap for reading ASE, write the alignment pair to the output. 
	    sys.stdout.write(line + "\n")
	    count += 1

sys.stderr.write("There are: " + str(len(SharedPairs.keys())) + " RBB pairs.\n")
sys.stderr.write("Pairs with at least one SNP/Indel: " + str(count) + "\n") 
sys.stderr.write("COMPLETE")
