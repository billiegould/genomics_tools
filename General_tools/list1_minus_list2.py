# -*- coding: utf-8 -*-
"""
Created on Mon Apr 14 11:54:51 2014

@author: Billie
"""
###usage : python overlap.py listfile1.txt listfile2.txt; Will record everything that is in list1 that is NOT in list2,  and writes to list1_uniques.txt

import sys


list1=open(sys.argv[1], "r")
list1pos=[]
for line in list1:
    list1pos.append(line)
list1.close()

list2=open(sys.argv[2], "r")
list2pos=[]
for line in list2:
    list2pos.append(line)
list2.close()

uniques=list(set(list1pos) - set(list2pos))
print "There are " + str(len(uniques)) + " unique entries in the first list."

output=open("list1_uniques.txt", "w")
for item in uniques:    
    output.write(item)
output.close()

print "complete, writing to list1_uniques.txt"        
