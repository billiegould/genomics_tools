## This script will take a genes only gff annotation file and generate a promoters only gff file.

import sys

#output = open("Mguttatus_promoters.gff", "w")
gff=open(sys.argv[1], "r")

for line in gff:
    scaff = line.split()[0]
    gene = line.split()[8]
    strand = line.split()[6]
    if strand == "+":
        gene_start = int(line.split()[3])
        prm_start = gene_start - 1000
    	sys.stdout.write('%s\t%s\t%s\t%d\t%d\t%s\t%s\t%s\t%s\n' % (scaff, "phytz.10", "promoter", prm_start, gene_start, ".", strand, ".", gene))
    if strand == "-":
        gene_start = int(line.split()[4])
        prm_start = gene_start + 1000
    	sys.stdout.write('%s\t%s\t%s\t%d\t%d\t%s\t%s\t%s\t%s\n' % (scaff, "phytz.10", "promoter", gene_start, prm_start, ".", strand, ".", gene))

sys.stderr.write("Conversion complete. \n")

gff.close()
#output.close()
