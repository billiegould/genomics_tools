# this script will go through a gff3 file of gene CDS regions and summ the total CDS length for each gene.

import sys

genes=open(sys.argv[1], "r")

prev_gene = "start"
cds_len_list = []

for line in genes:
    if "GENE" in line.split():
        continue

    gene = line.split()[8][0:18]
    sys.stderr.write(gene + "\n")
    if gene == prev_gene:
        length = int(int(line.split()[4]) - int(line.split()[3]))
        cds_len_list.append(length)
    elif gene != prev_gene:

        sum_len=sum(cds_len_list)
        sys.stdout.write(prev_gene +"\t" + str(sum_len) +"\n")

        length = int(int(line.split()[4]) - int(line.split()[3]))
        cds_len_list = [length]
        prev_gene = gene

genes.close()
sys.stderr.write("Complete.")