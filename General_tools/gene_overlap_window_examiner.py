# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 13:57:36 2015

@author: Billie
"""
# this script will compare fst outlier windows to a list of genes and find the overlaps.

#example input file format:
#scaff	win_start	win_end	G_stat	tot.depth
#scaffold_1	51	780	5.1475733143	290.0
#scaffold_1	782	1671	6.3073680467	130.0
#scaffold_1	1674	2330	9.08465530941	184.0

import sys

print( "Useage: script.py windowfile.txt gff.txt. . . .Remember to adjust window boundaries in the python script BEFORE RUNNING")


#create an dictionary of lists for outlier windows on each scaffold:
windict = {}

outlier_windows=open(sys.argv[1], "r")
for line in outlier_windows:
    if "win_start" in line.split():     #adjust content so that this skipps any header line present in the input.
        continue
    else:
        #print(line.split())
        scaffold = line.split()[0]

        win_start = line.split()[1]
        win_end = line.split()[2]
        win_stat = line.split()[4]	## define where the window statistic is stored in the input file
        window=[win_start, win_end, win_stat]

        #midpoint=int(line.split()[1])
        #window=[int(midpoint-1000), int(midpoint+1000)] #define the window according to distance from a midpoint

    if scaffold in windict:
        windict[scaffold].append(window)  #append window startand end to list
    else:
        windict[scaffold]=[window]      #create list #append window start and end and statistic to list

outlier_windows.close()
#print (windict)

#for each gene in the GFF, compare to outlier window list for the scaffold
output=open("output.txt", "w")
output.write("GENE" + "\t" + "CHR" + "\t" + "OVLP.WIN.START"+ "\t" + "WIN.STAT" + "\n")

genefile=open(sys.argv[2], "r")
for line in genefile:
    gene=line.split()
    #print(gene)
    genechr=gene[0]
    #genestart=int(gene[1])
    genestart=int(gene[3])
    #geneend = int(gene[2])
    geneend=int(gene[4])
    genename=gene[8]
    #genename=genestart
    if genechr in windict:
        windowlist=windict[genechr]
    else:
        windowlist=[]
#for each gene, see if it overlaps any windows, if not read in next gene and repeat
    for window in windowlist:
        windowstart = int(window[0])
        windowend = int(window[1])
        windowstat = window[2]
        if geneend < windowstart or genestart > windowend:
            continue
            #print "gene " + str(genename) + " non-overlapping with window " + str(windowstart)            
        elif genestart <= windowstart <= geneend:
            output.write(str(genename) + "\t" + str(genechr) + "\t" + str(windowstart)+ "\t" + str(windowstat) + "\n")
        elif genestart <= windowend <= geneend:
            output.write(str(genename) + "\t" + str(genechr) + "\t" + str(windowstart)+ "\t" + str(windowstat) + "\n")
        elif windowstart <= genestart and windowend >= geneend:
            output.write(str(genename) + "\t" + str(genechr) + "\t" + str(windowstart)+ "\t" + str(windowstat) + "\n")
output.close()
genefile.close()
print ("Done. printed to 'output.txt' ")
