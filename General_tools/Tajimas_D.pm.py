import sys
import math

# n = number of sequences (= 2*num indiv. for diploids)
# pi_list = list of pi values for each genotyped base in a region

def myTajD(n, pi_list):

    a1 = 0
    a2 = 0
    for i in range(1, int(n)): #sum is for up to N-1,since python does not include the last item in the range, we're covered here.
        a1 += 1/float(i)
        a2 += 1/(float(i)*float(i)) # see hartl and clark p. 175

    b1 = float(n+1)/(3*(n-1))       # see Tajima 1989
    b2 = float(2*(n*n +n+3))/(9*n*(n-1))

    c1 = (b1) - 1/a1
    c2 = b2-((float(n+2))/(a1*n))+(a2/(a1*a1))

    e1 = c1/a1
    e2 = c2/(a1*a1+a2)

    S=0

    region_pi=float(sum(pi_list))/float(len(pi_list))

    for p in pi_list:
        if 0 < p < 1:
            S=S+1           #total number of polymorphic sites in the window, S

    if S != 0:              #calculate Tajima's D
        theta = float(S)/a1
        #print("theta ", theta)
        d = float(region_pi *len(pi_list)) - theta ###ths is Tajima's D, raw. pi for the sequence is adjusted to a sum rather than a per site estimate
        p1 = e1*S
        p2 = e2*S*(S-1)
        denom=float(math.sqrt(p1+p2)) #denominator is the expected variance in d
        #print "sqrtvariance= " +str(denom)
        D = d/denom
        #print "TajD= " +str(D)
    else:
        theta = "no seg. sites"
        D = 'NA'
    return (S, D)
