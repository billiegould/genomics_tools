# -*- coding: utf-8 -*-
"""
Created on Mon Apr 14 11:54:51 2014

@author: Billie
"""
print "usage : python extractor_chr-pos.py datafile.txt targetlist_to_extract.txt. chromosome and position of marker must be in col 1 and col 2 in both files. both files must be SORTED. This will be really SLOW IF all the chr/pos in targetlist are not somewhere in the datalist. find the matches using overlap_byChr.py first"


import sys

#open list 1 for reading
data=open(sys.argv[1], "r")
datalist=[]
for line in data:
    datalist.append(line)

numlines = len(datalist)
data.close()
print "there are " + str(numlines) + " SNPs in the data file"
#open list 2 for reading
target_list=open(sys.argv[2], "r")

#create an output text file to write the results to
#matches=open("matches.txt", "w")
data=open(sys.argv[1], "r")
match_list=[]
ext = 0
for target in target_list:
    ext = ext + 1
    #print "processing target number: " + str(ext) + " target:" + str(target.split()[0:2])
    for i in range(0, numlines):
        dataline=data.readline()
        #print "target:"+ str(target.split())
        #print "dataline:" + str(list(dataline.split()[0:2]))
        if "#" in dataline:
            continue
        elif target.split()[0:2] == list(dataline.split()[0:2]):
            #print "MATCH"           
            match_list.append(dataline)
            break
       # else:
           # print str(dataline[0:2]) + " NO MATCH" +"\n"
            #continue

matches=open("matches.txt", "w")
for item in match_list:
    matches.write(str(item))
matches.close()
data.close()
target_list.close()


