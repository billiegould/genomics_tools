## This script will take a CDS region only gff annotation file and generate a file of non-CDS regions in the stdout
## requires a list of chromosome scaffold lengths in the working directory

import sys

gff=open(sys.argv[1], "r")

#open and read chrom lengths file into dictionary object
chr_len_dict={}
file = open("scaff_lengths2.txt", "r")
for line in file:
    scaffold = line.split()[0]
    #print scaffold
    length = line.split()[1]
    #print length
    chr_len_dict[scaffold]=length
sys.stderr.write( "Read scaffold lengths")
file.close()

prev_scaff = "start"
prev_CDS_end = 0

for line in gff:
    scaff = line.split()[0]
    #print(str(scaff == prev_scaff))
    CDS_end1 = int(line.split()[3])
    CDS_end2 = int(line.split()[4])
    CDS_start = min(CDS_end1, CDS_end2)
    CDS_end = max(CDS_end1, CDS_end2) 
    
    if scaff == prev_scaff:
	#print("scaff==prev_scaff")
        sys.stdout.write('%s\tphytozome\tnonCDS\t%s\t%s\n' % (scaff, prev_CDS_end, CDS_start))	#while in the same scaffold, write out noncoding interval before the exon
        prev_CDS_end = CDS_end
    
    elif scaff != prev_scaff and scaff in chr_len_dict:
        #print("scaff != prevscaff")
	chr_end_pos = chr_len_dict[prev_scaff]
        sys.stdout.write('%s\tphytozome\tnonCDS\t%s\t%s\n' % (prev_scaff,prev_CDS_end,chr_end_pos))	#write out the last interval for the previous scaffold
	prev_CDS_end = 0

	sys.stdout.write('%s\tphytozome\tnonCDS\t%s\t%s\n' % (scaff, prev_CDS_end, CDS_start))	#write out the first interval for the new scaffold
	prev_CDS_end = CDS_end
	prev_scaff = scaff

sys.stderr.write("Conversion complete.")

gff.close()
