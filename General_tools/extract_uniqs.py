# -*- coding: utf-8 -*-
"""
Created on Mon Nov 17 15:42:53 2014

@author: Billie
"""

### this script will take a single SORTED file of CHR/POS entries and extract unique entries.
## Useage: script inputlist.txt outfile

import sys

data=open(sys.argv[1], "r")
output=open(sys.argv[2], "w")
uniqlist=["start"]
prev_chrom=0
for line in data:
    chrom=int(line.split()[0])
    if chrom != prev_chrom:
        print "Finished processing Chromosome: " +str(prev_chrom)
        prev_chrom = chrom
    if line != uniqlist[-1]:
        output.write(line)
        uniqlist.append(line)
print "Finished processing Chromosome: " +str(chrom)
data.close()
output.close()

print "There are " + str(len(uniqlist)-1) + "unique positions in the list"
print "Complete. writen to file: " + str(sys.argv[2])
