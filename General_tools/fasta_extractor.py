# -*- coding: utf-8 -*-
"""
Created on Mon Apr 14 11:54:51 2014

@author: Billie
"""
print "usage :  this will extract fasta sequences from a large fasta file. script largefasta.fa targetstoget.txt. both files must be SORTED and REMOVE ANY HEADERS."


import sys

#open big fasta file an count the entries
data=open(sys.argv[1], "r")
datalist=data.read().splitlines()
numlines = len(datalist)
data.close()
print "there are " + str(numlines/2) + " sequences in the data file"

#open list of targets to extract from larger file
target_list=open(sys.argv[2], "r")

#for each target in the list, compare sequentially to sequences in the large fasta file
data=open(sys.argv[1], "r")
match_list=[]
ext = 0
for target in target_list:
    ext = ext + 1
    print "processing target number: " + str(ext)

    for i in range(0, numlines):
        dataline = str( data.readline() )
        if ">" in dataline:
            #print "target: " + target
            #print "dataline: " + dataline

            if str(target.strip()) in dataline:
                print "MATCH"
                sequence = data.readline()
                match_list.append(dataline)
                match_list.append(sequence)
                break  #move on to next target sequence
            else:
                #print ("NO MATCH" +"\n")
                continue
        else:
            continue
matches=open("matches.txt", "w")
for item in match_list:
    matches.write(str(item))
matches.close()
data.close()
target_list.close()