## This script will extract sequence regions from a fasta file in to a separate fasta. Use with reference genome fasta to extract the sequence of regions
# specified in a gene region file with the format:
# gene_name chrom   start   end

# Reference fasta file must be non-interleaved.

#Useage: script ref.fa gene_list.txt

import sys

genome = open(sys.argv[1], "r")

chrom_dict = {}

for line in genome:
    if ">" in line:
        chrom = line.strip()[1:]
        seq = genome.next().strip()
        chrom_dict[chrom]=[seq]
    else:
        continue
genome.close()
print("Genome read complete.")

output = open("sequences.fa", "w")
regions = open(sys.argv[2], "r")

for r in regions:
    name = r.split()[0]
    chrom = r.split()[1]
    start = int(r.split()[2]) - 1
    end = int(r.split()[3])

    chrom_seq = chrom_dict[chrom][0]
    print(chrom_seq)
    reg_seq = "".join(chrom_seq[start:end])

    output.write('>%s_%s_%s-%s\n%s\n' % (name, chrom, start, end, reg_seq))

print("Regions output to sequences.fa")

regions.close()
output.close()