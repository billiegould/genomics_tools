# -*- coding: utf-8 -*-
"""
Created on Mon Apr 14 11:54:51 2014

@author: Billie
"""
###usage : python overlap2.py listfile1.txt listfile2.txt match_column ; Will record everything that is in both list1 AND list2, matches.txt
## matches by values in column specified as argument 3.

import sys

col=int(sys.argv[3])

list1=open(sys.argv[1], "r")
list1pos=[]
for line in list1:
    list1pos.append(line.split()[col-1])
list1.close()

list2=open(sys.argv[2], "r")
list2pos=[]
for line in list2:
    list2pos.append(line.split()[col-1])
list2.close()

matches=list(set(list1pos) & set(list2pos))
print "There are " + str(len(matches)) + " matching entries."

output=open("matches.txt", "w")
for item in matches:    
    output.write(str(item) +"\n")
output.close()

print "complete, writing to matches.txt"        
