# Useage: script.py infile interval_list > outfile

#print NOTE: Run this script on one snape file per chromosome (snps only).

# before running, store genes file as pickle object, a stored python dictionary

import sys
import math
import Tajimas_D
import pickle

#load the dictionary of all genes
all_genes = open((sys.argv[2] + ".pkl"), 'rb')   #use the full path to pickled file here
genes_dict = pickle.load(all_genes)       #a stored dictionary of intervals per scaffold

#extract the genes specific to the scaffold of interest
snape_file = open(sys.argv[1], "r")
scaffold = snape_file.readline().split()[0]
snape_file.close()
genes = genes_dict[scaffold]

#sys.stdout.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % ("gene","scaff","start","end","length","IA.cov","IA.num.snps","IA.Taj.D", "CP.cov", "CP.num.snps", "CP.Taj.D"))

snape_file = open(sys.argv[1], "r")

n_IA = 184
n_CP = 204
n_all = 388

for g in genes:
    gene = g[0]
    #print gene
    gene_start = int(g[1])
    gene_end = int(g[2])
    gene_len = gene_end - gene_start

    pi_list_IA=[]
    pi_list_CP=[]
    pi_list_tot=[]

    for line in snape_file:
        pos = int(line.split()[1])

        if "*" in line.split():
            continue
        else:
            IA_ref_dp=int(line.split()[3])
            IA_alt_dp=int(line.split()[4])
            CP_ref_dp=int(line.split()[14])
            CP_alt_dp=int(line.split()[15])
            CP_dp = CP_ref_dp + CP_alt_dp
            IA_dp = IA_ref_dp + IA_alt_dp
            tot_dp = IA_dp + CP_dp

        if pos < gene_start:
            #print "skip" + str(pos)
            continue

        elif gene_start <= pos <= gene_end:
            if IA_dp < 50 or CP_dp < 50 or tot_dp >998:     # apply depth cutoffs
                continue
            else:
                IA_snp_prob = float(line.split()[8])
                CP_snp_prob = float(line.split()[19])

                if IA_snp_prob <= 0.05:
                    site_pi_IA = 0
                    IA_altAF = 0
                elif 0.05 < IA_snp_prob < 0.95: continue
                elif IA_snp_prob >= 0.95:
                    IA_altAF = float(line.split()[10])
                    site_pi_IA = float(n_IA/(n_IA-1))*(2.0 * IA_altAF * (1.0-IA_altAF))  #pi is equal to 2pq * (n/n-1) see Tajima 1989

                if CP_snp_prob <= 0.05:
                    site_pi_CP = 0
                    CP_altAF = 0
                elif 0.05 < CP_snp_prob < 0.95: continue
                elif CP_snp_prob >= 0.95:
                    CP_altAF = float(line.split()[21])
                    site_pi_CP = float(n_CP/(n_CP-1))*(2.0 * CP_altAF * (1.0-CP_altAF))  #pi is equal to 2pq * (n/n-1) see Tajima 1989

                if CP_snp_prob <= 0.05 and IA_snp_prob <= 0.05: site_pi_tot = 0
                elif 0.05 < CP_snp_prob < 0.95 or 0.05 < IA_snp_prob < 0.95: continue
                elif CP_snp_prob >= 0.95 or IA_snp_prob >= 0.95:
                    tot_altAF = (CP_altAF + IA_altAF)/2.0
                    site_pi_tot = float(n_all/(n_all-1))*(2.0 * tot_altAF * (1.0-tot_altAF))

                pi_list_IA.append(site_pi_IA)
                pi_list_CP.append(site_pi_CP)
                pi_list_tot.append(site_pi_tot)

        elif pos > gene_end:
            #print "CP_pi_list:" + str(pi_list_CP)
            #print "IA_pi_list:" + str(pi_list_IA)
            CP_gene_cov = len(pi_list_CP)/float(gene_end - gene_start)
            IA_gene_cov = len(pi_list_IA)/float(gene_end - gene_start)
            tot_gene_cov = len(pi_list_tot)/float(gene_end - gene_start)

            if not pi_list_IA:
                IA_S = TajD_IA = "NA"
            else:
                (IA_S, TajD_IA) = Tajimas_D.myTajD(n_IA, pi_list_IA)

            if not pi_list_CP:
                CP_S = TajD_CP = "NA"
            else:
                (CP_S, TajD_CP) = Tajimas_D.myTajD(n_CP, pi_list_CP)

            if not pi_list_tot:
                tot_S = TajD_tot = "NA"
            else:
                (tot_S, TajD_tot) = Tajimas_D.myTajD(n_all, pi_list_tot)

            sys.stdout.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (gene, scaffold, gene_start, gene_end, gene_len, IA_gene_cov, IA_S, TajD_IA, CP_gene_cov, CP_S, TajD_CP, tot_gene_cov, tot_S, TajD_tot))
            break

snape_file.close()
