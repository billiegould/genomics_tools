# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import sys
#sys.path.append("/Users/Billie/Desktop/PyVCF-master")

print "NOTE: Run this script on one snape file per chromosome (snps only). Useage: script.py infile. Writes to StdOut. "

#n=int(sys.argv[3])*2    #the number of individuals in the pool
n_IA = 184
n_CP = 204

snape_file = open(sys.argv[1], "r") #this file is the merged snape file:
#Input format: IA stats are listed in each row followed by CP stats
#CHR	POS	REF.ALLELE	 #REF.NUCL	  #ALT.NUCL	QUAL.REF	QUAL.ALT	MOST.FREQUENT.NUCL	1-PROB.0(prob of snp)	PROB.1	EXPECT.FREQ
#scaffold_1	1	T	35	0	72	72	T	0.0451	1.117e-138	0.0009034 scaffold_1	1	T	26	0	71	71	T	0.05264	5.553e-101	0.001206
#scaffold_1	2	A	35	0	72	72	A	0.0451	1.117e-138	0.0009034 scaffold_1	2	A	32	0	70	70	A	0.04716	1.406e-120	0.0009795
#scaffold_1	3	G	35	0	71	71	G	0.0451	3.537e-135	0.0009034 scaffold_1	3	G	34	0	70	70	G	0.04559	5.609e-128	0.0009212

#Pi_output = open("Pi_windows.txt", "w")
#Pi_output.write("scaff" + "\t" + "win.start" + "\t" + "win.end" + "\t" + "win.size" + "\t"+ "missing.sites" + "\t" + "num.snps" + "\t" + "avg.alt.freq"+ "\t" + "win.pi"  + "\n" )
sys.stdout.write("scaff" + "\t" + "win.start" + "\t" + "win.end" + "\t" + "freqCP" + "\t"+ "freqIA" + "\t" + "freq_diff" + "\t" + "piCP"+ "\t" + "piIA" + "\t" + "Dxy_win" +"\t"+ "missing_data" + "\t" + "win_depth" +"\n" )

pi_list_IA = []
pi_list_CP = []
freq_list_IA = []
freq_list_CP = []
dxy_list = []
depth_list=[]
positions = []
snps = 0

for line in snape_file:

    position = int(line.split()[1])
    #print("Processing position: " + str(position))

    try:
        last_field = line.split()[21]
        prob_snp_IA = float(line.split()[8])
        prob_snp_CP = float(line.split()[19])
    except IndexError: #this exception occurs if there are not enough fields or there is a "*" value at this position in the snape input file.
        continue

    if 0.05 < prob_snp_IA < 0.95 and 0.05 < prob_snp_CP < 0.95: #consider these positions as missing data
        continue
    else:
        freq_exp_IA = float(line.split()[10])  #expected allele frequencies, will be 0 for invariant reference sites
        freq_exp_CP = float(line.split()[21])

        site_pi_IA = 2 * freq_exp_IA * (1-freq_exp_IA) * (n_IA/(n_IA - 1))  #Tajima 1989
        site_pi_CP = 2 * freq_exp_CP * (1-freq_exp_CP) * (n_CP/(n_CP - 1))

        site_dxy = (freq_exp_CP * (1-freq_exp_CP)) + (freq_exp_IA * (1-freq_exp_CP)) #double check this in Nei 1987 book, eq 10.20

        site_depth = int(line.split()[3]) + int(line.split()[4]) + int(line.split()[14]) + int(line.split()[15])  #depth reference plus depth alt in IA and CP respectively.

        pi_list_IA.append(site_pi_IA)     #enter the value into the list, including zeros
        pi_list_CP.append(site_pi_CP)

        freq_list_IA.append(freq_exp_IA)
        freq_list_CP.append(freq_exp_CP)

        dxy_list.append(site_dxy)

        depth_list.append(site_depth)

        positions.append(position)
        if prob_snp_IA >= 0.95 or prob_snp_CP >= 0.95:
            snps += 1
            #print("snp count: " + str(snps))

    if snps == 100:     #Every 100 snps, calculate end position of window and avg. pi for window
        #print("Window snp positions: " + str(positions))
        scaff = line.split()[0]
        window_start = positions[0]
        window_end = positions[-1]
            
        window_freq_IA = float(sum(freq_list_IA))/len(freq_list_IA)  #window avg alt allele freq equals avg of all incl. fixed sites
        window_freq_CP = float(sum(freq_list_CP))/len(freq_list_CP)
        freq_diff = window_freq_IA - window_freq_CP

        window_pi_IA = float(sum(pi_list_IA))/len(pi_list_IA)  #window pi is equal to average of site pi across all sites in window, including invariant ones
        window_pi_CP = float(sum(pi_list_CP))/len(pi_list_CP)

        window_dxy = float(sum(dxy_list))/len(dxy_list)

        missing_data = (window_end - window_start) - len(pi_list_IA)

        window_depth = float(sum(depth_list))/len(depth_list)

        sys.stdout.write(str(scaff) + "\t" + str(window_start) + "\t" + str(window_end) + "\t" + str(window_freq_CP) +"\t"+ str(window_freq_IA) + "\t" + str(freq_diff) + "\t" + str(window_pi_CP) + "\t" + str(window_pi_IA) + "\t" + str(window_dxy) + "\t"+ str(missing_data) +"\t" + str(window_depth)+ "\n" )

        pi_list_IA=[]
        pi_list_CP=[]
        freq_list_IA=[]
        freq_list_CP=[]
        dxy_list=[]
        depth_list=[]
        positions=[]
        snps = 0
#get the last window
if snps > 10:
    scaff = line.split()[0]
    window_start = positions[0]
    window_end = positions[-1]

    window_freq_IA = float(sum(freq_list_IA))/len(freq_list_IA)  #window avg alt allele freq equals avg of all incl. fixed sites
    window_freq_CP = float(sum(freq_list_CP))/len(freq_list_CP)
    freq_diff = window_freq_IA - window_freq_CP

    window_pi_IA = float(sum(pi_list_IA))/len(pi_list_IA)  #window pi is equal to average of site pi across all sites in window, including invariant ones
    window_pi_CP = float(sum(pi_list_CP))/len(pi_list_CP)

    window_dxy = float(sum(dxy_list))/len(dxy_list)

    missing_data = (window_end - window_start) - len(pi_list_IA)

    window_depth = float(sum(depth_list))/len(depth_list)

    sys.stdout.write(str(scaff) + "\t" + str(window_start) + "\t" + str(window_end) + "\t" + str(window_freq_CP) +"\t"+ str(window_freq_IA) + "\t" + str(freq_diff) + "\t" + str(window_pi_CP) + "\t" + str(window_pi_IA) + "\t" + str(window_dxy) + "\t" + str(missing_data) + "\t" + str(window_depth) + "\n" )

snape_file.close()
#print( "Completed pi windows for scaffold " + str(scaff) +"\t" )