# this script will find fixed SNPs in a merged snpae file

import sys


snape_file=open(sys.argv[1], "r")

for line in snape_file:
    if "*" in line.split():
        continue

    else:
        IA_Ref = int(line.split()[3])
        IA_Alt = int(line.split()[4])
        CP_Ref = int(line.split()[14])
        CP_Alt = int(line.split()[15])

        if (IA_Ref + IA_Alt) < 50 or (CP_Ref + CP_Alt) < 50:
            continue
        elif (IA_Ref + IA_Alt + CP_Ref + CP_Alt) > 998:
            continue
        if IA_Ref == 0 and CP_Alt == 0:
            sys.stdout.write("IA_fixed" + "\t" + line + "\n")
        elif CP_Ref == 0 and IA_Alt == 0:
            sys.stdout.write("CP_fixed" + "\t" + line + "\n")

sys.stderr.write("Complete")



# this is going to be conservative if fixation necesitates 0 ALT reads in one pool (seq error and high depth makes this impos
        # do enrichment tests of this data