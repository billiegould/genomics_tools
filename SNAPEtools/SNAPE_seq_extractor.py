## This script will extract a consensus sequence for each pool in a merged SNAPE file for Mguttatus.

## Merged SNAPE file format:
## scaffold_1	1	T	35	0	72	72	T	0.0451	1.117e-138	0.0009034 scaffold_1	1	T	26	71	71	T	0.05264	5.553e-101	0.001206
## scaffold_1	2	A	35	0	72	72	A	0.0451	1.117e-138	0.0009034 scaffold_1	2	A	32	70	70	A	0.04716	1.406e-120	0.0009795
## scaffold_1	3	G	35	0	71	71	G	0.0451	3.537e-135	0.0009034 scaffold_1	3	G	34	70	70	G	0.04559	5.609e-128	0.0009212
## scaffold_1	4	T	35	0	71	71	T	0.0451	3.537e-135	0.0009034 scaffold_1	4	T	35	70	70	T	0.04485	1.12e-131	0.0008944
## scaffold_1	5	G	36	0	71	71	G	0.04438	5.61e-139	0.0008781 scaffold_1	5	G	35	71	71	G	0.04485	3.538e-135	0.0008944
## scaffold_1	6	T	36	0	72	72	T	0.04438	1.408e-142	0.0008781 scaffold_1	6	T	37	71	71	T	0.04343	8.902e-143	0.0008451
## scaffold_1	7	C	36	0	72	72	C	0.04438	1.408e-142	0.0008781 scaffold_1	7	C	36	70	70	CA	0.9984	9.164e-131	0.0527


import sys

iupac_dict = {"A":"A", "T":"T", "C":"C", "G":"G", "AG":"R", "GA":"R", "CT":"Y", "TC":"Y", "GC":"S", "CG":"S", "AT":"W", "TA":"W", "GT":"K", "TG":"K", "AC":"M", "CA":"M"}

region=open(sys.argv[1], "r")

snape_file=open(sys.argv[2], "r")

sequences=open("sequences.fa", "w")

for line in region:
    region_name = line.split()[0]
    print("starting gene: " + region_name)
    region_start = int(line.split()[1])
    region_end = int(line.split()[2])
    IA_seq = []
    CP_seq = []
    prev_pos = 0

    for line in snape_file:
        pos = int(line.split()[1])
        ref = line.split()[2]

        if pos < region_start:
            prev_pos = pos
            continue
        elif pos > region_end:
            print(">" + region_name + "_IA" + "\n" + "".join(IA_seq) + "\n")
            print(">" + region_name + "_CP" + "\n" + "".join(CP_seq) + "\n")
            sequences.write(">" + region_name + "_IA" + "\n" + "".join(IA_seq) + "\n")
            sequences.write(">" + region_name + "_CP" + "\n" + "".join(CP_seq) + "\n")
            prev_pos = pos
            print("Finished gene: " + region_name)
            break
        elif region_start < pos < region_end:
            try:
                IA_call = line.split()[7]
                CP_call = line.split()[18]
            except IndexError:
                IA_base = ref
                CP_base = ref
            else:

                #determine IA base
                if float(line.split()[8]) >0.95:
                    IA_base = iupac_dict[IA_call]
                elif 0.05 < float(line.split()[8])< 0.95:
                    IA_base = ref
                elif float(line.split()[8]) < 0.05:
                    IA_base = ref

                #determine CP base
                if float(line.split()[19]) >0.95:
                    CP_base = iupac_dict[CP_call]
                elif 0.05 < float(line.split()[19])< 0.95:
                    CP_base = ref
                elif float(line.split()[19]) < 0.05:
                    CP_base = ref

            ## append base and adjust for gaps
            if pos > (prev_pos+1):
                IA_seq.append("N"*(pos-prev_pos-1))
                IA_seq.append(IA_base)
                CP_seq.append("N"*(pos-prev_pos-1))
                CP_seq.append(CP_base)
            elif pos == (prev_pos+1):
                IA_seq.append(IA_base)
                CP_seq.append(CP_base)

            prev_pos = pos

print "Written to >>> sequences.fa <<<<"
sequences.close()
snape_file.close()
region.close()
