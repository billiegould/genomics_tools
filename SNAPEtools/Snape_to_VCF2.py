import sys
import math

#print "NOTE: Run this script on one snape file per chromosome (snps only). Useage: script.py infile. Writes to StdOut. "


snape_file = open(sys.argv[1], "r") #this file is the merged snape file:
#Input format: IA stats are listed in each row followed by CP stats
#CHR	POS	REF.ALLELE	 #REF.NUCL	  #ALT.NUCL	QUAL.REF	QUAL.ALT	MOST.FREQUENT.NUCL	1-PROB.0(prob of snp)	PROB.1	EXPECT.FREQ
#scaffold_1	1	T	35	0	72	72	T	0.0451	1.117e-138	0.0009034 scaffold_1	1	T	26	0	71	71	T	0.05264	5.553e-101	0.001206
#scaffold_1	2	A	35	0	72	72	A	0.0451	1.117e-138	0.0009034 scaffold_1	2	A	32	0	70	70	A	0.04716	1.406e-120	0.0009795
#scaffold_1	3	G	35	0	71	71	G	0.0451	3.537e-135	0.0009034 scaffold_1	3	G	34	0	70	70	G	0.04559	5.609e-128	0.0009212


err_base = 0

for line in snape_file:
    #print line
    if "*" in line.split():
        continue


    DP_IA = int(line.split()[3]) + int(line.split()[4])
    DP_CP = int(line.split()[14]) + int(line.split()[15])
    tot_depth = DP_IA + DP_CP
    if DP_IA <= 50 or DP_CP <= 50 or tot_depth > 998:       #filter out low and high coverage snps
        #print "Low or High depth"
        continue


    prob_snp_IA = float(line.split()[8])
    prob_snp_CP = float(line.split()[19])
    if prob_snp_IA < 0.95 and prob_snp_CP < 0.95:           #consider these positions as missing data or reference called bases
        #print "Missing data or Reference base call"
        continue

    elif prob_snp_IA >= 0.95 or prob_snp_CP >= 0.95:     # For Variable bases only:
        CHR = line.split()[0]
        POS = line.split()[1]
        ID = "."
        QUAL = "."
        FILTER = "PASS"
        INFO = "."
        FORMAT = "GT:DP"

        IA_snREF = line.split()[2]
        CP_snREF = line.split()[13]
        if CP_snREF != IA_snREF:                                    # check for mismatching reference bases
            print "Position: " + str(CHR) + "\t" + str(POS)
            sys.exit("ERROR: Mis-matching reference bases.")

        IA_alleles = list(line.split()[7])
        CP_alleles = list(line.split()[18])
        all_alleles = set(CP_alleles) | set(IA_alleles)
        #print "all alleles: " +str(all_alleles)
        alt_alleles = [allele for allele in all_alleles if allele != CP_snREF]
        #print "ALT alleles: " +str(alt_alleles)

        #if len(alt_alleles) > 1:                                    # skip non-biallelic sites
         #   print "Non biallelic."

        if not alt_alleles:                                       # if there are no alternate alleles, count as error, skip line
            err_base += 1
            #sys.stderr.write(line)
            #sys.exit("Error: Alt alleles expected but not found")
            continue
        else:
            vcfALT = alt_alleles[0]

    vcf_line=[CHR,POS,ID,CP_snREF,vcfALT,QUAL,FILTER,INFO,FORMAT, ("1/1:" + str(DP_CP)), ("1/1:" + str(DP_IA))]       # ***  define all "gt" calls as 1/1 for the pool.
    print ("\t".join(vcf_line))

stderr.write("Chr: " + str(CHR) + "\t"+ "Error: Alt alleles expected but not found: " + str(err_base) +"\n")
snape_file.close()