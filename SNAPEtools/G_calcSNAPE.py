# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 17:06:04 2015

@author: Billie
"""

import sys
import math
## this script will take a merged SNAPE file and calculate the G statistic for 100 SNP windows. Based on script by Kevin Wright.
#see Magwene et al. 2011

#Input file format:
#CHR	POS	REF.ALLELE	 #REF.NUCL	  #ALT.NUCL	QUAL.REF	QUAL.ALT	MOST.FREQUENT.NUCL	1-PROB.0(prob of snp)	PROB.1	EXPECT.FREQ . . repeat
#scaffold_1	1	T	35	0	72	72	T	0.0451	1.117e-138	0.0009034 scaffold_1	1	T	26	0	71	71	T	0.05264	5.553e-101	0.001206
#scaffold_1	2	A	35	0	72	72	A	0.0451	1.117e-138	0.0009034 scaffold_1	2	A	32	0	70	70	A	0.04716	1.406e-120	0.0009795
#scaffold_1	3	G	35	0	71	71	G	0.0451	3.537e-135	0.0009034 scaffold_1	3	G	34	0	70	70	G	0.04559	5.609e-128	0.0009212

#output=open("G_windows.txt", "w")
#output.write("scaff" +"\t"+ "win_start" +"\t"+ "win_end" +"\t" + "G_stat" +"\n")
sys.stdout.write("scaff" +"\t"+ "win_start" +"\t"+ "win_end" +"\t" + "G_stat" +"\t"+ "avg.win.depth" +"\n")

snapefile=open(sys.argv[1], "r")
G_list = []
Positions = []
depthlist = []

for line in snapefile:
    #print(line)
    try:
        last_field = line.split()[21]
    except IndexError: #this exception occurs if there is a "*" value at this position in the snape input file and not enough fields.
        continue

    scaff=line.split()[0]
    pos=line.split()[1]
    #allele count per population
    a0_pop1 = float(line.split()[3]) #coverage_ReferanceAllele_Pop1
    #print (str(a0_pop1))
    a1_pop1 = float(line.split()[4]) #covAltPop1
    #print (str(a1_pop1))
    a0_pop2 = float(line.split()[14]) #covRefPop2
    #print (str(a0_pop2))
    a1_pop2 = float(line.split()[15]) #covAltPop2
    #print (str(a1_pop2))

    depthPop1 = a0_pop1 + a1_pop1
    #print ("Pop 1 depth: " + str(depthPop1))
    depthPop2 = a0_pop2 + a1_pop2
    #print ("Pop 2 depth: " + str(depthPop2))
    tot_depth = depthPop1 + depthPop2
        
    if depthPop1>=50 and depthPop2>=50:        
        #calc frequency of each allele across both pops
        freqa0 = ( a0_pop1 + a0_pop2 ) / (depthPop1 + depthPop2)  #frequency of REF allele in combined pop
        freqa1 = ( a1_pop1 + a1_pop2 ) / (depthPop1 + depthPop2) #frequency of ALT allele in combined pop
     #   print ("overall frqs: " +str(freqa0) + "\t" + str(freqa1))
        #expected frequency if no difference between pops
        expect_a0p1 = freqa0 * depthPop1
        expect_a1p1 = freqa1 * depthPop1
        expect_a0p2 = freqa0 * depthPop2
        expect_a1p2 = freqa1 * depthPop2
     #   print ("Expected freqs pop1:R/A pop2:R/A : " + str(expect_a0p1) +"\t"+ str(expect_a1p1) +"\t"+ str(expect_a0p2) +"\t"+ str(expect_a1p2))
         #1 Calc G Statistic: Ratio of observed to expected.
         #filter based on expected frequency.
        if expect_a0p1 > 2 and expect_a1p1 > 2 and expect_a0p2 > 2 and expect_a1p2 > 2: #cutoff recommended in the Magwene 2011 paper. 

            if a0_pop1 != 0:
                G_a0p1 = a0_pop1 * math.log(a0_pop1/expect_a0p1)
            else: 
                G_a0p1 = 0
		
            if a1_pop1 != 0:
                G_a1p1 = a1_pop1 * math.log(a1_pop1/expect_a1p1)
            else:
                G_a1p1 = 0
		
            if a0_pop2 != 0:
                G_a0p2 = a0_pop2 * math.log(a0_pop2/expect_a0p2)
            else:
                G_a0p2 = 0 
			
            if a1_pop2 != 0:
                G_a1p2 = a1_pop2 * math.log(a1_pop2/expect_a1p2)
            else:
                G_a1p2 = 0
			
            G = 2 * (G_a0p1 + G_a1p1 + G_a0p2 + G_a1p2)

            G_list.append(G)
            #print("G-vals: " + str(G_list))
            Positions.append(pos)
            depthlist.append(tot_depth)

            # when 100 snp G values have been calculated, take an average for the window and write to file. Reset lists
            if len(G_list) % 100 == 0:

                #print("number of valid snps: " + str(len(G_list)))
                #print("snp positions: " + str(Positions))
                #print("G-vals: " + str(G_list))

                win_avg = sum(G_list)/float(len(G_list))
                win_start = Positions[0]
                win_end = Positions[-1]
                win_depth = sum(depthlist)/float(len(depthlist))
                #output.write(str(scaff) +"\t"+ str(win_start) +"\t"+ str(win_end) +"\t" + str(win_avg) +"\n")
                sys.stdout.write(str(scaff) +"\t"+ str(win_start) +"\t"+ str(win_end) +"\t" + str(win_avg) + "\t" + str(win_depth) + "\n")

                G_list = []
                Positions = []
                depthlist = []

# get the last window value if it contains at least 10 left over snps
if len(G_list) >= 10:
    #print("number of valid snps: " + str(len(G_list)))
    #print("snp positions: " + str(Positions))
    #print("G-vals: " + str(G_list))

    win_avg = sum(G_list)/float(len(G_list))
    win_start = Positions[0]
    win_end = Positions[-1]
    win_depth = sum(depthlist)/float(len(depthlist))
    #output.write(str(scaff) +"\t"+ str(win_start) +"\t"+ str(win_end) +"\t" + str(win_avg) +"\n")
    sys.stdout.write(str(scaff) +"\t"+ str(win_start) +"\t"+ str(win_end) +"\t" + str(win_avg) + "\t" + str(win_depth) + "\n")

#print( "Complete. Printed to G_windows.txt.")
snapefile.close()
#output.close()