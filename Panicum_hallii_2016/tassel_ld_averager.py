# This script will take Tassel tabular output of pairwise LD measures and calculate average LD for each snp in the file. Window size is set in Tassel.
# input format:
# locus1    position1   site1   numberofstates  states1 frequency1  position2   site2   numberof states2    states2 freq2   distbp  r^2 DPrime  pDiseq  N

import sys

data = open(sys.argv[1], "r")

prev_pos = 0
snp_lds = []
win_snps = 0

for line in data:
    #print(line)
    if "Locus1" in line:
	continue
    else:
        chrom = line.split()[0]
        ld = float(line.split()[13])
        current_pos = int(line.split()[1])

        if current_pos == prev_pos:
            win_snps += 1
            snp_lds.append(ld)
            prev_pos = current_pos

        else:
            #print("LDs: " + str(win_snps) + " : " +str(snp_lds) +"\n")
            if len(snp_lds) != 0:
	    	snp_avg_ld = sum(snp_lds)/len(snp_lds)
            	sys.stdout.write("%s\t%s\t%s\n" % (chrom, current_pos, snp_avg_ld))
            
	    snp_lds = [ld]
            win_snps = 1
            prev_pos = current_pos

data.close() 
sys.stderr.write("Complete")
