# This script will take the table of site genotype depth values and out put sites to remove from vcf based on depth cutoffs.

import sys

depths = open(sys.argv[1], "r")

sites = open("low_cov_sites_92.txt", "w")

for line in depths:

    chrom = line.split()[0]
    pos = line.split()[1]
    good_calls = 0

    for i in range(2,94,1):
        if int(line.split()[i]) >= 8:
            good_calls =+ 1

    if good_calls < 3:
        sites.write(chrom + "\t" + pos + "\n")
    else:
        continue

sites.close()
depths.close()

# There may be other ways to do this with awk.