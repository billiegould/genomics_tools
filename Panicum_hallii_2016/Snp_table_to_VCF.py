# This script will import the JGI table of raw genotype base calls and output it to VCF format for downstream analyses.

import sys



#write header
vcf = open("Phallii_snps_106.vcf", "w")

vcf.write("#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	AOZXB	NNUO	AOZXA	NNUN	APBGU	NNUP	NNUS	NNUT	NNUU	NNUW	NUOY	NZWC	NZWG	NZWN	NZWO	NZWP	NZWS	NZWT	NZWU	NZWW	NZXS	NZWX	NZWY	NZXT	NZXU	NZXW	NZXX	NZXY	PAGX	PAGY	UAGG	UAGH	UAGN	UAGO	UAGP	UAGS	UAGT	UAGU	UAGW	UAGX	UAGY	UAGZ	UAHA	UAHB	UAHC	UAHG	UAHH	UAHN	WZGS	WZGT	WZGU	WZGW	WZGX	WZGY	WZGZ	WZHA	WZHB	PIN1	CNF2	LAR1	POI3	IAUP	IAUS	IAUX	IAUY	IAUZ	IAWN	IAWO	IAWP	IAWS	IAWU	IAWX	YAZH	AOZWN	AOZWO	AOZWP	AOZWS	AOZWT	AOZWU	AOZWW	AOZWX	AOZWY	AOZWZ	IFZO	EPS1	AOZXC	AOZXG	ICII	COR18	ATO2F	CDM5F	CHR2F	CUT1F	ERO2F	HTE1F	HUA2F	MAP3F	MEZ1F	MOR1F	NDD5F	PTZ1F	SAP3F	TEM7F	TLA6F	TZI1F \n")


#open table file and transpose genotype info to vcf

infile = open(sys.argv[1], "r")

for line in infile:
    site_list = []
    chrom = line.split()[0]
    pos = line.split()[1]
    ref = line.split()[2]
    alt = line.split()[3]

    site_list.extend([chrom, pos, ".", ref, alt, ".", ".", ".", "GT"])

    for i in range(4,109,1):

        if line.split()[i] == ref:
            call = "0/0"

        elif line.split()[i] == alt:
            call = "1/1"

        elif line.split()[i] == "N":
            call = "./."

        else:
            call = "0/1"

        site_list.append(call)

    #print(site_list)
    vcf.write( "\t".join(site_list) + "\n")

vcf.close()
infile.close()

