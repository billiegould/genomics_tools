# This script will calculate a matrix of pairwise Dxy values for samples in a vcf

import sys
import vcf
import numpy


def get_Dxy(genotypes1, genotypes2):
    genotypes = zip(genotypes1,genotypes2)
    called_sites = [i for i,j in genotypes if (i != None and j != None)]
    #print(called_sites)
    diff_sites = [ a for a,b in genotypes if (a != None and b != None and a != b)]
    #print(diff_sites)
    Dxy_pair = len(diff_sites)/float(len(called_sites))

    return(numpy.round(Dxy_pair, 3))


###################
vcf = vcf.Reader(open(sys.argv[1], "r"))

genotype_dict = {}
for s in vcf.samples:
    genotype_dict[s] = []

for record in vcf:
    for s in vcf.samples:
#        sys.stderr.write("Reading in sample: " + str(s) + "\n")
        call = record.genotype(s)
        genotype_dict[s].append(call.gt_type)

#print {s:genotype_dict[s][0:5] for s in genotype_dict.keys()}


samples = vcf.samples
n = len(samples)

my_array = numpy.zeros((n,n))

sys.stderr.write("Calculating pairwise Dxy . . .\n")

for i, sample_1 in enumerate(samples):
    for j, sample_2 in enumerate(samples):
        if j >= i:
            break
        
        genotypes1 = genotype_dict[sample_1]
        genotypes2 = genotype_dict[sample_2]
        Dxy = get_Dxy(genotypes1, genotypes2)

        my_array[i,j] = Dxy
        my_array[j,i] = Dxy

header = " ".join(samples)

numpy.savetxt(fname = "Pairwise_Dxy_Matrix.txt", X = my_array, delimiter = " ", header=header)
#print(header)
#print(my_array)

sys.stderr.write("Dxy array saved to Pairwise_Dxy_Matrix.txt \n")
