# This script will merge a file containing depth on each genomic site with a VCF file contianing snp calls at variable sites. VCF Output can be used to
#adjust genomic calculations for the number of callable sites in a region.

#filter vcf down to 89 lines
# remove the header from the vcf and store as file.
# input headerless vcf to script
# extract reference sequence for transband region into file and use as argv3 input to script

# useage: script in.vcf in.depth in.ref.seq

import sys
#from itertools import izip_longest

out_vcf = open("transband_all_snp_sites.vcf", "w")

# read in the reference sequence as a single string
ref_seq = open(sys.argv[3], "r")
sequence = ref_seq.readline()
ref_seq.close()

#check
print("Ref Seq: " + sequence[0:20])

# create dictionary of data lines indexed by posistion from depth file
depth_data = open(sys.argv[2], "r")
depth_dict = {}

for line in depth_data:
    #print(line)
    depth_pos = int(line.split()[1])
    ref_base = sequence[(depth_pos - 4347580)]
    total_reads = sum(map(int, line.split()[2:]))

    #print("Pos: "+str(depth_pos)+ " Reads "+str(total_reads))

    if total_reads >= 8:
        gts = []
        for d in map(int, line.split()[2:]):
            if d >= 8: gts.append("0/0:"+str(d))
            elif d < 8: gts.append("./.:"+str(d))
        alt_base = ref_base

    elif total_reads < 8:
        gts = ["./."] * 89
        alt_base = "."

    data = ["Chr03", depth_pos, ".", ref_base, alt_base, ".", ".", ".", "."] + gts
    depth_dict[depth_pos] = data

depth_data.close()


#check
print("Depth data: " +"\n")
print(depth_dict[4347580])
print(depth_dict[4347581])


# open vcf. for line in vcf; write out all depth lines at positions between seq(prev_vcf_pos:vcf_pos)
prev_pos = 4347579
vcf = open(sys.argv[1], "r")

for line in vcf:
    if "#" in line: out_vcf.write(line);continue

    vcf_pos = int(line.split()[1])
    for i in list(range((prev_pos+1),vcf_pos,1)):            # does not include snp at current position
        try:
            out_vcf.write("\t".join(map(str, depth_dict[i])) + "\n")
        except KeyError:
            continue

    # write vcf line to file
    out_vcf.write(line)
    # prev_vcf_pos = vcf_pos
    prev_pos = vcf_pos

#output the rest of the depth data
for j in list(range((vcf_pos+1),5188631,1)):
    out_vcf.write("\t".join(map(str, depth_dict[i])) + "\n")

out_vcf.close()
vcf.close()
